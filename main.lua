local mod = RegisterMod("Sodom and Gomorrah", 1)
mod.Version = 1.12

game = Game()
sfx = SFXManager()

XalumMods = XalumMods or {} -- This is dumb and I don't like it anymore but I'll keep it for back-compat

local ExistingSaveData
if XalumMods.SodomAndGomorrah and XalumMods.SodomAndGomorrah.savedata then
	ExistingSaveData = XalumMods.SodomAndGomorrah.savedata
end

XalumMods.SodomAndGomorrah = mod
SodomAndGomorrah = mod

if ExistingSaveData then
	mod.savedata = ExistingSaveData
end

include("lua.utils.savedata")

mod.PLAYER_TYPE = {
	SODOM		= Isaac.GetPlayerTypeByName("Sodom"),
	GOMORRAH	= Isaac.GetPlayerTypeByName("Gomorrah"),
	SODOM_B		= Isaac.GetPlayerTypeByName("Sodom", true),
	GOMORRAH_B	= Isaac.GetPlayerTypeByName("Gomorrah", true),
	BELA		= Isaac.GetPlayerTypeByName("Bela"),

	SODOM2		= Isaac.GetPlayerTypeByName(" Sodom"),
}

mod.FAMILIAR = {
	CHAINED_HEART	= Isaac.GetEntityVariantByName("Chained Heart"),
	BELA			= Isaac.GetEntityVariantByName("Bela"),
	ADMAH			= Isaac.GetEntityVariantByName("Admah"),
	ZEBOIM			= Isaac.GetEntityVariantByName("Zeboim"),
	ADMAH_B 		= Isaac.GetEntityVariantByName("Tainted Admah"),
	ZEBOIM_B		= Isaac.GetEntityVariantByName("Tainted Zeboim"),
}

mod.EFFECT = {
	GHOSTS				= Isaac.GetEntityVariantByName("Sodom Gomorrah Ghosts"),
	CHAINED_HEART_CHAIN	= Isaac.GetEntityVariantByName("Chained Heart Chain"),
	BELA				= Isaac.GetEntityVariantByName("Bela (Effect)"),
	BELA_FIRE			= Isaac.GetEntityVariantByName("Bela Fire"),
	POSITION_TARGET		= Isaac.GetEntityVariantByName("Sodom Position Target"),
}

mod.ITEM = {
	STOP 			= Isaac.GetItemIdByName("Stop"),
	SWITCH 			= Isaac.GetItemIdByName("Switch"),

	SODOMS_HEART 	= Isaac.GetItemIdByName("Sodom's Heart"),

	TAG_SODOM 		= Isaac.GetItemIdByName("TAG_SODOM"),
	TAG_GOMORRAH 	= Isaac.GetItemIdByName("TAG_GOMORRAH"),
	TAG_SWITCH_ACTIVE = Isaac.GetItemIdByName("TAG_SWITCH_ACTIVE"),
	TAG_BELAS_BIRTHRIGHT = Isaac.GetItemIdByName("TAG_BELAS_BIRTHRIGHT"),
}

-- Legacy Variables
mod.STOP	= Isaac.GetItemIdByName("Stop")
mod.SWITCH	= Isaac.GetItemIdByName("Switch")

mod.SPRITESHEET = {
	SODOM			= "gfx/characters/costumes/sodom/",
	GOMORRAH		= "gfx/characters/costumes/gomorrah/",

	SODOM_B			= "gfx/characters/costumes/sodom_b/",
	GOMORRAH_B		= "gfx/characters/costumes/gomorrah_b/",
}

mod.SOUND = {
	STOP_ENABLE		= Isaac.GetSoundIdByName("Stop Enable"),
	STOP_DISABLE	= Isaac.GetSoundIdByName("Stop Disable"),
}

mod.CHALLENGE = {
	ONLY_SODOM		= Isaac.GetChallengeIdByName("[S+G] Pillar of Salt"),
	ISAACS_HEART 	= Isaac.GetChallengeIdByName("[S+G] Cardiac Arrest"),
	B_SIDE_CHAINED	= Isaac.GetChallengeIdByName("[S+G] Bound by Steel"),
}

mod.CURRENT_DIMENSION	= 0
mod.PREVIOUS_DIMENSION	= 0

mod.INFAMY_TINT = Color(1, 1, 1, 1, 0.25, 0.25, 0.5)

mod.MAUS_DOOR_DAMAGE_FLAGS = DamageFlag.DAMAGE_SPIKES + DamageFlag.DAMAGE_INVINCIBLE + DamageFlag.DAMAGE_NO_MODIFIERS + DamageFlag.DAMAGE_NO_PENALTIES

mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, continued)
	local player = Isaac.GetPlayer()
	mod.CheckedGomorrah = false

	if not continued then
		local itempool = game:GetItemPool()

		if game.Challenge == mod.CHALLENGE.ONLY_SODOM or (mod.savedata.challenge_mode == 2 and mod.IsPlayerSodom(p1)) then
			if not mod.IsPlayerSodom(player) then
				player:ChangePlayerType(mod.PLAYER_TYPE.SODOM2)
				player:GetSprite():Play("WalkDown")
			end

			player:AddCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART)

			player:SetPocketActiveItem(mod.ITEM.STOP, ActiveSlot.SLOT_POCKET, false)

			itempool:RemoveCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART)
			itempool:RemoveCollectible(CollectibleType.COLLECTIBLE_CLICKER)
		end

		if game.Challenge == mod.CHALLENGE.ISAACS_HEART or (mod.savedata.challenge_mode == 3 and mod.IsPlayerSodom(p1)) then
			if not mod.IsPlayerSodom(player) then
				player:ChangePlayerType(mod.PLAYER_TYPE.SODOM2)
				player:GetSprite():Play("WalkDown")
			end

			player:AddCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART)

			itempool:RemoveCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART)
			itempool:RemoveCollectible(CollectibleType.COLLECTIBLE_CLICKER)
		end

		if game.Challenge == mod.CHALLENGE.B_SIDE_CHAINED or (mod.savedata.challenge_mode == 4 and mod.IsPlayerSodom(p1)) then
			if not mod.IsPlayerSodom(player) then
				player:ChangePlayerType(mod.PLAYER_TYPE.SODOM_B)
				player:GetSprite():Play("WalkDown")
			end

			itempool:RemoveCollectible(CollectibleType.COLLECTIBLE_CLICKER)
		end

		if mod.IsPlayerSodom(player) then
			local itempool = game:GetItemPool()

			for _, value in pairs(mod.SodomAndGomorrahItemBlacklist) do
				itempool:RemoveCollectible(value)
			end

			for _, value in pairs(mod.SodomAndGomorrahTrinketBlacklist) do
				itempool:RemoveTrinket(value)
			end
		
			if player:GetPlayerType() == mod.PLAYER_TYPE.SODOM_B then
				for _, value in pairs(mod.SodomAndGomorrahItemBlacklistB) do
					itempool:RemoveCollectible(value)
				end
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, function(_, will_save)
	if will_save then
		for _, player in pairs(Isaac.FindByType(1)) do
			player = player:ToPlayer()

			if mod.IsPlayerGomorrah(player) then
				local data = player:GetData()

				if data.sodom then
					player.Parent = data.sodom
				end
			elseif player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
				if player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_GOMORRAH) then
					local data = player:GetData()

					if data.sodom then
						player.Parent = data.sodom
					end
				end
			end
		end
	end

	mod.CURRENT_DIMENSION = nil
	mod.CURRENT_LEVEL = nil
end)

mod:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pickup, collider)
	if collider:ToPlayer() then
		if game.Challenge == mod.CHALLENGE.ONLY_SODOM then
			mod.savedata.beat_pillar_of_salt = true
			game:GetHUD():ShowItemText("Unlocked challenge mode", "Check the dss menu")
		elseif game.Challenge == mod.CHALLENGE.ISAACS_HEART then
			mod.savedata.beat_cardiac_arrest = true
			game:GetHUD():ShowItemText("Unlocked challenge mode", "Check the dss menu")
		elseif game.Challenge == mod.CHALLENGE.B_SIDE_CHAINED then
			mod.savedata.beat_bound_by_steel = true
			game:GetHUD():ShowItemText("Unlocked challenge mode", "Check the dss menu")
		end
	end
end, PickupVariant.PICKUP_TROPHY)

function mod.IsChallengePillarOfSalt() return game.Challenge == mod.CHALLENGE.ONLY_SODOM or (game.Challenge == 0 and mod.savedata.challenge_mode == "pillar of salt") end
function mod.IsChallengeCardiacArrest() return game.Challenge == mod.CHALLENGE.ISAACS_HEART or (game.Challenge == 0 and mod.savedata.challenge_mode == "cardiac arrest") end
function mod.IsChallengeBoundBySteel() return game.Challenge == mod.CHALLENGE.B_SIDE_CHAINED or (game.Challenge == 0 and mod.savedata.challenge_mode == "bound by steel") end

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function()
	if Isaac.GetPlayer():GetPlayerType() == mod.PLAYER_TYPE.SODOM then
		local itempool = game:GetItemPool()

		for _, value in pairs(mod.SodomAndGomorrahTrinketBlacklist) do
			itempool:RemoveTrinket(value)
		end
	end
end, CollectibleType.COLLECTIBLE_MOMS_BOX)

function mod.IsValueInList(id, list)
	for _, value in pairs(list) do
		if value == id then return true end
	end

	return false
end

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	local p1 = Isaac.GetPlayer()

	if mod.IsPlayerSodom(p1) then
		if mod.IsValueInList(pickup.SubType, mod.SodomAndGomorrahItemBlacklist) then
			pickup:Morph(5, 100, 0, true, true, true)
		end

		if mod.IsPlayerSodom(p1, false, true) then
			if mod.IsValueInList(pickup.SubType, mod.SodomAndGomorrahItemBlacklistB) then
				pickup:Morph(5, 100, 0, true, true, true)
			end
		end
	end
end, 100)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	mod.PREVIOUS_DIMENSION = mod.CURRENT_DIMENSION
	mod.CURRENT_DIMENSION = mod.GetDimensionID()

	mod.CURRENT_ROOM_INDEX = game:GetLevel():GetCurrentRoomIndex()
end)

function mod.ChangePlayerAnm2(player, key, forcebody, alt)
	local sprite = player:GetSprite()
	local data = player:GetData()
	local anm2 = "gfx/player_sodom_gomorrah.anm2"
	local bside = mod.IsPlayerSodomGomorrah(player, false, true)
	local sodom = data.sodom or player

	local anim = sprite:GetAnimation()
	local frame = sprite:GetFrame()
	local overlay = sprite:GetOverlayAnimation()
	local frame2 = sprite:GetOverlayFrame()

	if bside then
		anm2 = "gfx/player_sodom_gomorrah_b.anm2"
	end

	sprite:Load(anm2, true)

	local ghost = mod.GetLostCurseStatus(player)

	local path = mod.SPRITESHEET[string.upper(key)]

	local skin = mod.savedata and mod.savedata.skin_a or "default"
	local tooth = mod.GetToothAndNailStatus(player)
	local canfly = player.CanFly and not ghost
	local hung = mod.HasUsedPinkingShears(player) or player:HasCollectible(CollectibleType.COLLECTIBLE_TRANSCENDENCE)
	local pony = sodom:HasCollectible(CollectibleType.COLLECTIBLE_PONY) or sodom:HasCollectible(CollectibleType.COLLECTIBLE_WHITE_PONY)

	path = path .. skin .. "/character_" .. string.lower(key)
	if tooth then path = path .. "_tooth" end

	local flightmode = hung and "_hung" or pony and "_pony" or canfly and "_fly"
	if flightmode and not forcebody then path = path .. flightmode end

	if bside then
		if player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
			sprite:ReplaceSpritesheet(13, "gfx/characters/costumes/bela.png")

			path = path .. "_lit"
			if alt then
				path = path .. "2"
			end
		else
			sprite:ReplaceSpritesheet(13, "gfx/characters/costumes/no_bela.png")
			path = path .. "_unlit"
		end
	end

	if not bside and player:GetData().stopactive then
		path = path .. "_inactive"

		--sprite:ReplaceSpritesheet()
	end

	path = path .. ".png"

	for i = 0, 12 do
		sprite:ReplaceSpritesheet(i, path)
	end

	sprite:LoadGraphics()
	sprite:SetFrame(anim, frame)
	sprite:SetOverlayFrame(overlay, frame2)

	if data.position_indicator then
		local spr = data.position_indicator:GetSprite()

		if ghost then
			spr:Play("Ghost")
		else
			if bside then
				if player:GetData().active then
					spr:Play("Lit")
				else
					if mod.IsPlayerSodom(player) then
					spr:Play("SodomB")
					else
						spr:Play("GomorrahB")
					end
				end
			else
				if mod.IsPlayerSodom(player) then
					spr:Play("Sodom")
				else
					spr:Play("Gomorrah")
				end
			end
		end
	end
end

function mod.CountBits(mask)
	local count = 0
	while mask ~= 0 do
	 	count = count + 1
		mask = mask & mask - 1
	end

	return count
end

function mod.GetClosest(pos, typ, var, sub)
	local ents = Isaac.FindByType(typ or -1, var or -1, sub or -1)

	local closest
	local dist = 99999

	for _, ent in pairs(ents) do
		local d = pos:Distance(ent.Position)

		if d < dist then
			closest = ent
			dist = d
		end
	end

	return closest
end

function mod.CalculatePocketSize(player)
	local n = 1
	if player:HasCollectible(CollectibleType.COLLECTIBLE_STARTER_DECK) or player:HasCollectible(CollectibleType.COLLECTIBLE_POLYDACTYLY) or player:HasCollectible(CollectibleType.COLLECTIBLE_LITTLE_BAGGY) then
		n = 2
	end

	return n
end

function mod.CalculateTrinketPocketSize(player)
	local n = 1
	if player:HasCollectible(CollectibleType.COLLECTIBLE_MOMS_PURSE) or player:HasCollectible(CollectibleType.COLLECTIBLE_BELLY_BUTTON) then
		n = 2
	end

	return n
end

function mod.GetDimensionID()
	local level = game:GetLevel()
	local id = level:GetCurrentRoomIndex()
	local hash = GetPtrHash(level:GetRoomByIdx(id))

	for i = 0, 2 do
		if hash == GetPtrHash(level:GetRoomByIdx(id, i)) then return i end
	end
end

--[[function mod.IsInDimension(num)
	local level = game:GetLevel()
	local id = level:GetCurrentRoomIndex()

	return GetPtrHash(level:GetRoomByIdx(id)) == GetPtrHash(level:GetRoomByIdx(id, num or 0))
end

function mod.IsMirrorStage()
	local level = game:GetLevel()
	return level:GetAbsoluteStage() == LevelStage.STAGE1_2 and level:GetStageType() >= StageType.STAGETYPE_REPENTANCE
end

function mod.IsInMirror() return mod.IsMirrorStage() and mod.IsInDimension(1) end]]

function mod.Lerp(init, target, percentage)	return init + (target - init) * percentage end

function mod.DidDimensionJustChange()		return mod.CURRENT_DIMENSION ~= mod.PREVIOUS_DIMENSION end
function mod.IsTrinketInPocket(player, id)	return player:GetTrinket(0) == id and 0 or player:GetTrinket(1) == id and 1 end
function mod.DamageIsSacrifice(flags)		return game:GetRoom():GetType() == RoomType.ROOM_SACRIFICE and flags & DamageFlag.DAMAGE_SPIKES > 0 end
function mod.DamageIsMausDoor(flags, src)	return flags == mod.MAUS_DOOR_DAMAGE_FLAGS and src.Type == 0 and src.Variant == 0 end
function mod.HasCard(player, id)			return player:GetCard(0) == id or player:GetCard(1) == id end
function mod.CalcPlayerHealth(player)		return player:GetHearts() + player:GetSoulHearts() + player:GetBoneHearts() + player:GetEternalHearts() - player:GetRottenHearts() end
function mod.IsPocketEmpty(player, slot)	return player:GetCard(slot or 0) == 0 and player:GetPill(slot or 0) == 0 end
function mod.CanPlayerPickBattery(player)	for i = 0, 2 do if player:NeedsCharge(i) then return true end end end

function mod.GetLostCurseStatus(player)		return player:GetEffects():HasNullEffect(NullItemID.ID_LOST_CURSE) end
function mod.GetToothAndNailStatus(player)	return player:GetEffects():HasNullEffect(NullItemID.ID_TOOTH_AND_NAIL) end
function mod.HasUsedPinkingShears(player)	return player:GetEffects():HasCollectibleEffect(CollectibleType.COLLECTIBLE_PINKING_SHEARS) end

function mod.SodomGomorrahHasInfamy(heart)	return heart.Player:HasCollectible(CollectibleType.COLLECTIBLE_INFAMY) or heart.Player:GetData().gomorrah:HasCollectible(CollectibleType.COLLECTIBLE_INFAMY) end

include("lua.dss.deadseascrolls")
include("lua.dss.changelogs")

-- include("lua.challenges")
include("lua.utils.completion")

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	if not mod.GameLoaded then
		mod.LoadSaveData()
	end
end)

include("lua.core")
include("lua.a-side")
include("lua.b-side")

include("lua.utils.card_blacklister")
include("lua.utils.giantbooks")
include("lua.compatibility")

mod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	if mod.GameLoaded then
		mod.SaveSaveData()
	end
end)

print("Sodom and Gomorrah v" .. mod.Version .. " loaded successfully")