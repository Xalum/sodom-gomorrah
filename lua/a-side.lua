local mod = XalumMods.SodomAndGomorrah

-- NOTES FOR WORKSHOP:
--[[
	Reviver & blacklist tables
	Save & Continue doesn't work
]]

-- TODO:
--[[
	Soul of Isaac deletes boss room items

	-= Actual new shit =-
	B-side unlock note
]]

mod.DRIFT_MULTIPLIER = 0.1
mod.TURN_MULTIPLIER = 2.7
mod.TURN_THRESHOLD = 14
mod.AOE_SIZE_MULTIPLIER = 3.5
mod.MAX_CHAIN_LENGTH = 140

mod.SODOM_GOMORRAH_DAMAGE_FLAG_MASK = DamageFlag.DAMAGE_INVINCIBLE | DamageFlag.DAMAGE_ISSAC_HEART | DamageFlag.DAMAGE_PITFALL | DamageFlag.DAMAGE_IV_BAG
mod.SLOT_MACHINE_DAMAGE_VARIANT_MASK = (1 << 2) | (1 << 5) | (1 << 15) | (1 << 17)

mod.SodomAndGomorrahItemBlacklist = {
	CollectibleType.COLLECTIBLE_ARIES,			-- All movement is performed while not able to collide with enemies. Item's function literally impossible to use
	CollectibleType.COLLECTIBLE_GUPPYS_COLLAR,	-- Sodom/Gomorrah may not both decide to revive, can lead to major bugs
	CollectibleType.COLLECTIBLE_ESAU_JR,		-- Doesn't work at all, to a laughable degree
	CollectibleType.COLLECTIBLE_SUPLEX,			-- Cannot collide with npcs during the period in which you would have normally grabbed them

	CollectibleType.COLLECTIBLE_MEGA_MUSH, 		-- Idk if the item actually works but the visuals are fucked and we just haven't tried to fix them yet
	CollectibleType.COLLECTIBLE_HOST_HAT,		-- The shoot-back mechanic just kinda wouldn't work
	CollectibleType.COLLECTIBLE_PAUSE,
}

mod.SodomAndGomorrahTrinketBlacklist = { -- Reviver Trinkets can't really work since both players needs to hold them, but gomorrah's trinkets are transferred to sodom
	TrinketType.TRINKET_BROKEN_ANKH,
	TrinketType.TRINKET_MYSTERIOUS_PAPER,
	TrinketType.TRINKET_MISSING_POSTER,
}

mod.SodomAndGomorrahRevivers = {
	CollectibleType.COLLECTIBLE_1UP,
	CollectibleType.COLLECTIBLE_DEAD_CAT,
	CollectibleType.COLLECTIBLE_INNER_CHILD,
	CollectibleType.COLLECTIBLE_SPIRIT_SHACKLES,

	CollectibleType.COLLECTIBLE_LAZARUS_RAGS,
	CollectibleType.COLLECTIBLE_ANKH,
	CollectibleType.COLLECTIBLE_JUDAS_SHADOW,

	CollectibleType.COLLECTIBLE_HEARTBREAK,
}

mod.SodomGomorrahMainOnlyItems = {
	CollectibleType.COLLECTIBLE_STARTER_DECK,
	CollectibleType.COLLECTIBLE_LITTLE_BAGGY,
	CollectibleType.COLLECTIBLE_POLYDACTYLY,

	CollectibleType.COLLECTIBLE_SCHOOLBAG,

	CollectibleType.COLLECTIBLE_MOMS_PURSE,
	CollectibleType.COLLECTIBLE_BELLY_BUTTON,

	CollectibleType.COLLECTIBLE_BATTERY,
	CollectibleType.COLLECTIBLE_4_5_VOLT,
	CollectibleType.COLLECTIBLE_9_VOLT,
	CollectibleType.COLLECTIBLE_CHARGED_BABY,

	CollectibleType.COLLECTIBLE_MARBLES,

	CollectibleType.COLLECTIBLE_ECHO_CHAMBER,
}

mod.BannedGomorrahActives = {
	CollectibleType.COLLECTIBLE_BLANK_CARD,
	CollectibleType.COLLECTIBLE_PLACEBO,
	CollectibleType.COLLECTIBLE_BLANK_RUNE,
	CollectibleType.COLLECTIBLE_STITCHES,
	CollectibleType.COLLECTIBLE_RED_KEY,
	
	CollectibleType.COLLECTIBLE_BOBS_ROTTEN_HEAD, -- Thowables + Throwable-Adjacents
	CollectibleType.COLLECTIBLE_FRIEND_BALL,
	CollectibleType.COLLECTIBLE_DEAD_SEA_SCROLLS,
	CollectibleType.COLLECTIBLE_CANDLE,
	CollectibleType.COLLECTIBLE_RED_CANDLE,
	CollectibleType.COLLECTIBLE_ERASER,
	CollectibleType.COLLECTIBLE_DECAP_ATTACK,
}

mod.ForcedGomorrahActives = {
	CollectibleType.COLLECTIBLE_PONY,
	CollectibleType.COLLECTIBLE_WHITE_PONY,
	CollectibleType.COLLECTIBLE_BERSERK,
	CollectibleType.COLLECTIBLE_PLAN_C,
}

mod.SingleUseActives = {
	CollectibleType.COLLECTIBLE_DIPLOPIA,
	CollectibleType.COLLECTIBLE_EDENS_SOUL,
	CollectibleType.COLLECTIBLE_MAMA_MEGA,
	CollectibleType.COLLECTIBLE_MYSTERY_GIFT,
	CollectibleType.COLLECTIBLE_SACRIFICIAL_ALTAR,
	CollectibleType.COLLECTIBLE_FORGET_ME_NOW,
	CollectibleType.COLLECTIBLE_PLAN_C,
	CollectibleType.COLLECTIBLE_BLUE_BOX, -- Pandora's Box
	CollectibleType.COLLECTIBLE_ALABASTER_BOX,
	CollectibleType.COLLECTIBLE_DAMOCLES,
	CollectibleType.COLLECTIBLE_DEATH_CERTIFICATE,
	CollectibleType.COLLECTIBLE_GENESIS,
	CollectibleType.COLLECTIBLE_R_KEY,
}

mod.CHAINED_HEART_GFX_PATH = {
	NULL		= "blank.png",
	MANTLE_1	= "gfx/characters/costumes/costume_313_holymantle.png",
	MANTLE_2	= "gfx/characters/costumes/costume_313_holymantle_glow.png",
	SHADOWS		= "gfx/familiars/chained_heart_bookofshadows.png",

	DEFAULT		= "gfx/familiars/chained_heart.png",
	GNAWED_LEAF = "gfx/familiars/chained_heart_leaf.png",
	TOOTH_NAIL	= "gfx/familiars/chained_heart_tooth.png",
	BLUE_CHAINS = "gfx/familiars/chained_heart_spirit_shackles.png",
}

function mod.CanGomorrahUseItem(item)
	if mod.IsItemRemovedOnUse(item) then return false end

	for _, value in pairs(mod.BannedGomorrahActives) do
		if value == item then return false end
	end
	
	return true
end

function mod.ShouldGomorrahForceUseItem(item)
	for _, value in pairs(mod.ForcedGomorrahActives) do
		if value == item then return true end
	end
	
	return false
end

function mod.IsItemRemovedOnUse(item)
	for _, value in pairs(mod.SingleUseActives) do
		if value == item then return true end
	end
	
	return false
end

function mod.ShouldGomorrahTryPillCard(sodom)
	local prevent = mod.IsPocketEmpty(sodom, 0)
				 or sodom:GetCard(0) == Card.CARD_QUESTIONMARK
				 or sodom:GetCard(0) == Card.CARD_SOUL_LAZARUS

	return not prevent
end

function mod.RatifyRevivers(sodom, gomorrah)
	if not mod.DidDimensionJustChange() then
		for _, item in pairs(mod.SodomAndGomorrahRevivers) do
			local n1 = sodom:GetCollectibleNum(item)
			local n2 = gomorrah:GetCollectibleNum(item)

			if n1 > n2 then
				gomorrah:AddCollectible(item, 0, false)
			elseif n1 < n2 then
				sodom:AddCollectible(item, 0, false)
			end
		end
	end
end

function mod.CheckReplacerItems(player)
	if not mod.IsChallengePillarOfSalt() and not mod.IsChallengeCardiacArrest() then
		local n = player:GetCollectibleNum(CollectibleType.COLLECTIBLE_ISAACS_HEART)

		for i = 1, n do
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART, true)
			player:AddCollectible(mod.ITEM.SODOMS_HEART)
		end
	end
end

function mod.MakeWhiteFireGhost(player, removecorpse, stopsound)
	local fire = Isaac.Spawn(33, 4, 0, Vector.Zero, Vector.Zero, nil)

	player:TakeDamage(0, 0, EntityRef(fire), 0)

	fire:Remove()
	if removecorpse then
		for _, body in pairs(Isaac.FindByType(1000, 6)) do
		    if body.FrameCount <= 1 then
				body:Remove()
			end
		end
	end

	if stopsound then
		sfx:Stop(SoundEffect.SOUND_FIREDEATH_HISS)
	end
end

function mod.LostCurseParity(me, you)
	if not (me and you) then return end

	if mod.GetLostCurseStatus(me) and not mod.GetLostCurseStatus(you) then
		mod.MakeWhiteFireGhost(you, true)
	end
end

function mod.SpiritShackleParity(me, you)
	if not (me and you) then return end

	local m_effects = me:GetEffects()
	local y_effects = you:GetEffects()

	if y_effects:HasNullEffect(NullItemID.ID_SPIRIT_SHACKLES_DISABLED) and not m_effects:HasNullEffect(NullItemID.ID_SPIRIT_SHACKLES_DISABLED) then
		you:AddSoulHearts(1)
	end
end

function mod.DidToothAndNailJustChange(player)
	local data = player:GetData()
	local tooth = mod.GetToothAndNailStatus(player)

	if data.toothynail ~= tooth then
		data.toothynail = tooth

		return true
	end
end

function mod.IsPlayerStompy(player)
	return  player:HasPlayerForm(PlayerForm.PLAYERFORM_STOMPY) or
			player:HasCollectible(CollectibleType.COLLECTIBLE_LEO) or
			player:HasCollectible(CollectibleType.COLLECTIBLE_THUNDER_THIGHS)
end

function mod.CollectPlayerItems(player)
	local config = Isaac.GetItemConfig()
	local max = config:GetCollectibles().Size

	local items = {}
	local n

	for i = 1, max do
		n = player:GetCollectibleNum(i, true)
		if n > 0 then table.insert(items, {i, n}) end
	end

	local i = (1 << 32) - 1
	while config:GetCollectible(i) do
		n = player:GetCollectibleNum(i, true)
		if n > 0 then table.insert(items, {i, n}) end
		i = i - 1
	end

	return items
end

function mod.AssassinateGomorrah(sodom, reanm2, forcegomorrah)
	local gomorrah = forcegomorrah or sodom:GetData().gomorrah
	if not gomorrah then return end

	local loot = mod.CollectPlayerItems(gomorrah)

	local check = {}
	for _, item in pairs(mod.SodomAndGomorrahRevivers) do
		check[item] = true
	end

	for _, val in pairs(loot) do
		for i = 1, val[2] do
			if check[val[1]] then
				gomorrah:RemoveCollectible(val[1], true)
			else
				sodom:AddCollectible(val[1], 0, false)
			end
		end
	end


	gomorrah:Update() -- Removing revival items and calling :Die() on the same frame crashes the game

	gomorrah:ChangePlayerType(sodom:GetPlayerType())
	gomorrah.Parent = sodom
	gomorrah:GetSprite():Load("gfx/player_nodeath.anm2", true)

	gomorrah:Die()
	gomorrah:GetSprite():Update()
	gomorrah:Update()

	sodom:GetData().gomorrah = nil

	if false and reanm2 then
		sodom:ChangePlayerType(sodom:GetPlayerType())
	end
end

local function shouldLimitChainLength(characterIsTainted)
	return ((
		mod.IsChallengeBoundBySteel() or
		not characterIsTainted) and
		not mod.IsChallengePillarOfSalt()
	)
end

local function isPickupValidMagnetismTarget(entity)
	return (
		entity.Variant == PickupVariant.PICKUP_TRINKET or
		entity.Variant == PickupVariant.PICKUP_TAROTCARD or
		entity.Variant == PickupVariant.PICKUP_PILL or
		entity.Variant == PickupVariant.PICKUP_BROKEN_SHOVEL or (
			entity.Variant == PickupVariant.PICKUP_COLLECTIBLE and
			entity.SubType ~= 0
	))
end

local function getClosestMagnetismTarget(player)
	local pickups = Isaac.FindInRadius(player.Position, 30, EntityPartition.PICKUP)
	local closest = nil
	local distance = 9e9

	for _, entity in pairs(pickups) do
		if isPickupValidMagnetismTarget(entity) and entity.Position:Distance(player.Position) < distance then
			closest = entity
			distance = entity.Position:Distance(player.Position)
		end
	end

	return closest
end

function mod.IsJumping(player)
	return player:GetData().jump_velocity
end

function mod.DoJump(player, movement, tainted)
	local data = player:GetData()

	if player.SpriteOffset.Y - player.MoveSpeed * data.jump_velocity > 0 then
		data.jump_velocity = nil
		mod.DoJumpLand(player, tainted)
	else
		player.SpriteOffset = Vector(0, player.SpriteOffset.Y - player.MoveSpeed * data.jump_velocity / 2)
		data.jump_velocity = data.jump_velocity - player.MoveSpeed / 2
		data.lastvel = data.lastvel or Vector.Zero

		local driftMultiplier = mod.DRIFT_MULTIPLIER
		if player.Velocity:Length() < mod.DRIFT_MULTIPLIER * mod.TURN_THRESHOLD then
			driftMultiplier = mod.DRIFT_MULTIPLIER * mod.TURN_MULTIPLIER
		end
        player.Velocity = data.lastvel + movement * driftMultiplier

        if movement:Length() == 0 and mod.savedata.pickup_magnet == 1 then
        	local targetItem = getClosestMagnetismTarget(player)
        	if targetItem then
        		local targetPosition = mod.Lerp(player.Position, targetItem.Position, 0.1)
        		local targetVelocity = targetPosition - player.Position

        		player.Velocity = mod.Lerp(data.lastvel, targetVelocity, 0.1)
        	end
        end


        mod.CheckSodomGomorrahGridCollision(player)

		if shouldLimitChainLength(tainted) then
			mod.LimitJumpingChainLength(player, data.gomorrah or data.sodom)
		end

		mod.CheckMoverBrimstones()
	end
end

function mod.DoJumpLand(player, tainted)
	player.GridCollisionClass = player:GetData().sodom_gomorrah_old_gridcoll or EntityGridCollisionClass.GRIDCOLL_GROUND
	player.SpriteOffset = Vector.Zero
	player.Velocity = Vector.Zero

	for _, entity in pairs(Isaac.GetRoomEntities()) do
		if entity:IsVulnerableEnemy() or mod.IsWhiteListedForJumpLand(entity) then
			if entity.Position:Distance(player.Position) <= entity.Size + player.Size * mod.AOE_SIZE_MULTIPLIER then
				if entity.Type == 19 and (entity.Variant == 2 or entity.Variant == 3) and entity:ToNPC().I2 ~= 1 then
					local pass = true

					if entity.Variant == 3 and entity:GetSprite():GetAnimation() == "HeadRotate" then
						pass = false
					end

					if pass then
						sfx:Play(SoundEffect.SOUND_BONE_BREAK, 1, 0, false, 0.8)
						entity:ToNPC().I2 = 1

						for i = 1, math.random(2, 3) do
							Isaac.Spawn(1000, 4, 0, entity.Position, RandomVector():Resized(math.random(3)), entity)
						end
					end
				end 

				local dmg = player.Damage * mod.GetPlayerLandingDamageMultiplier(player)
				entity:TakeDamage(dmg, 0, EntityRef(player), 15)
			end
		end
	end

	if not player.CanFly then
		local room = game:GetRoom()
		local grid = room:GetGridEntityFromPos(player.Position)

		if grid and mod.CanSodomGomorrahBreakGrid(grid) then
			grid:Destroy()
		end

		local pitchdown = mod.IsPlayerGomorrah(player)
		sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 1, 0, false, pitchdown and 0.7 or 1)

		if mod.IsPlayerStompy(player) then
			local shockwave = Isaac.Spawn(1000, 61, 0, player.Position, Vector.Zero, player):ToEffect()
			shockwave.Parent = player
			shockwave:SetRadii(20, 40)
			shockwave:SetTimeout(10)

			game:ShakeScreen(7)
		end

		mod.SpawnLandingEffects(player)
	end

	if tainted then
		local data = player:GetData()
		local active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

		if active then
			data.landinglag = 5
		else
			data.landinglag = 20
		end
	end
end

function mod.LimitJumpingChainLength(mover, other)
	local tpos = mover.Position + mover.Velocity

	if tpos:Distance(other.Position) > mod.MAX_CHAIN_LENGTH then
		local vect = (other.Position - mover.Position):Resized(0.1)
		
		repeat
			mover.Velocity = mover.Velocity + vect
		until other.Position:Distance(mover.Position + mover.Velocity) <= mod.MAX_CHAIN_LENGTH
	end
end

function mod.LimitStationaryChainLength(sodom, gomorrah)
	if not (sodom and gomorrah) then return end

	local distance = sodom.Position:Distance(gomorrah.Position)

	if distance > mod.MAX_CHAIN_LENGTH then
		sodom.Velocity = (gomorrah.Position - sodom.Position):Resized(1/3)
		gomorrah.Velocity = (sodom.Position - gomorrah.Position):Resized(1/3)
	end
end

function mod.CheckMover(player, movement, tainted)
	local mover, other
	if tainted then
		mover = player
	else
		mover, other = mod.ShouldSodomOrGomorrahMove(player, player:GetData().gomorrah, movement)
	end

	local mover_data = mover:GetData()

	if tainted then
		if mover_data.sg_target then
			mover_data.sg_target:Remove()
		end
	else
		other.Velocity = Vector.Zero
	end

	if tainted and not mover:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
		if mod.IsPlayerSodom(mover, true) then
			mover_data.jump_velocity = 12
		else
			mover_data.jump_velocity = 9
		end
	else
		mover_data.jump_velocity = 12
	end
	mover.Velocity = movement * mover.MoveSpeed * 2
	mover_data.lastvel = mover.Velocity

	mover_data.sodom_gomorrah_old_gridcoll = mover.GridCollisionClass
	mover.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
end

function mod.CheckMoverBrimstones()
	for _, brimstone in pairs(Isaac.FindByType(7, 1)) do
		if brimstone.Parent and brimstone.Parent:ToPlayer() and brimstone.Parent:GetData().jump_velocity then
			brimstone:ToLaser().DisableFollowParent = true
		end
	end
end

function mod.SummonGomorrah(player)
	local data = player:GetData()

	local gomorrah
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		if p:GetPlayerType() == mod.PLAYER_TYPE.GOMORRAH and p.ControllerIndex == player.ControllerIndex then
			gomorrah = p
			break
		end
	end

	if not gomorrah then
		Isaac.ExecuteCommand("addplayer " .. mod.PLAYER_TYPE.GOMORRAH .. " " .. player.ControllerIndex)
		gomorrah = Isaac.GetPlayer(game:GetNumPlayers() - 1)
	end

	data.gomorrah = gomorrah

	mod.ChangePlayerAnm2(data.gomorrah, "gomorrah")

	data.gomorrah:GetData().sodom = player

	local position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, data.gomorrah.Position, Vector.Zero, data.gomorrah)
	position_indicator.Visible = false
	position_indicator:GetSprite():Play("Gomorrah")

	data.gomorrah:GetData().position_indicator = position_indicator

	data.gomorrah.ControlsEnabled = false

	player.Position = player.Position + Vector(-40, 0)
	data.gomorrah.Position = player.Position + Vector(80, 0)

	if mod.IsChallengeCardiacArrest() then
		data.gomorrah:AddCollectible(CollectibleType.COLLECTIBLE_ISAACS_HEART)
	end

	mod.AssignPlayerHUDs()
end

function mod.GetPlayerLandingDamageMultiplier(player)
	if mod.IsPlayerGomorrah(player) then
		return 5
	else
		return 3
	end
end

function mod.CanSodomGomorrahBreakGrid(grid)
	local typ = grid:GetType()
	return  typ == GridEntityType.GRID_ROCK or
			typ == GridEntityType.GRID_ROCKT or
			typ == GridEntityType.GRID_ROCK_BOMB or
			typ == GridEntityType.GRID_ROCK_ALT or
			typ == GridEntityType.GRID_SPIDERWEB or
			typ == GridEntityType.GRID_TNT or
			typ == GridEntityType.GRID_POOP or
			typ == GridEntityType.GRID_STATUE or
			typ == GridEntityType.GRID_ROCK_SS or
			typ == GridEntityType.GRID_ROCK_SPIKED or
			typ == GridEntityType.GRID_ROCK_ALT2 or
			typ == GridEntityType.GRID_ROCK_GOLD
end

function mod.DoesSodomGomorrahCollideGrid(grid)
	local typ = grid and grid:GetType()
	return  grid and (
		grid.CollisionClass == GridCollisionClass.COLLISION_PIT or
		(typ == GridEntityType.GRID_LOCK and grid.State == 0) or
		typ == GridEntityType.GRID_ROCKB or
		typ == GridEntityType.GRID_PILLAR
	)
end

function mod.CheckGridSoftlockStatus(player)
	local data = player:GetData()
	data.sodomgomorrah_softlocktimer = data.sodomgomorrah_softlocktimer or 0

	local room = game:GetRoom()
	local grid = room:GetGridEntityFromPos(player.Position)

	if not player.CanFly and mod.DoesSodomGomorrahCollideGrid(grid) then
		data.sodomgomorrah_softlocktimer = data.sodomgomorrah_softlocktimer + 1
	else
		data.sodomgomorrah_softlocktimer = 0
	end

	if data.sodomgomorrah_softlocktimer > 420 then
		local hud = game:GetHUD()
		hud:ShowItemText("Pit Anti-Softlock Triggered", "Sorry about that")

		data.sodomgomorrah_softlocktimer = 0

		data.softlock_avoiding = true
		player:AddCacheFlags(CacheFlag.CACHE_FLYING)
		player:EvaluateItems()
	end
end

function mod.IsWhiteListedForJumpLand(entity)
	if entity.Type == 33 and entity.Variant <= 1 then
		return true
	end
end

function mod.IsDross(level)
	local stage = level:GetAbsoluteStage()
	local typ = level:GetStageType()

	return typ == StageType.STAGETYPE_REPENTANCE_B and stage == LevelStage.STAGE1_1 or stage == LevelStage.STAGE1_2
end

function mod.IsFloodedOrDownpour(level)
	local stage = level:GetAbsoluteStage()
	local typ = level:GetStageType()

	local downpour = typ == StageType.STAGETYPE_REPENTANCE and stage == LevelStage.STAGE1_1 or stage == LevelStage.STAGE1_2
	local flooded = typ == StageType.STAGETYPE_AFTERBIRTH and stage == LevelStage.STAGE2_1 or stage == LevelStage.STAGE2_2

	return downpour or flooded
end

function mod.IsWomb(level)
	local stage = level:GetAbsoluteStage()

	return stage == LevelStage.STAGE4_1 or stage == LevelStage.STAGE4_2
end

function mod.SpawnLandingEffects(player)
	local level = game:GetLevel()
	local room = game:GetRoom()

	local wet = room:HasWater()
	local splashcolor = mod.IsFloodedOrDownpour(level) and "blue" or mod.IsDross(level) and "green" or mod.IsWomb(level) and "red"

	if wet then
		local temp = {
			[3] = "gfx/effects/landingsplash_ground_",
			[4] = "gfx/effects/landingsplash_foreground_",
		}

		for i = 3, 4 do
			local splash = Isaac.Spawn(1000, 16, i, player.Position, Vector.Zero, player):ToEffect()

			splash.SpriteScale = Vector(0.3, 0.3)
			splash.Color = Color(1, 1, 1, 0.5)

			if splashcolor ~= "red" then
				local sprite = splash:GetSprite()
				sprite:ReplaceSpritesheet(0, temp[i] .. splashcolor .. ".png")
				sprite:LoadGraphics()
			end
		end
	else
		for i = 1, math.random(3, 5) do
			local dust = Isaac.Spawn(1000, 59, 0, player.Position, RandomVector():Resized(math.random(3)), player):ToEffect()
			dust:SetTimeout(50)

			dust.Color = Color(1, 0.8, 0.8, 0.15)
			dust.SpriteScale = Vector(0.75, 0.75)
			dust.RenderZOffset = -5000
		end
	end
end

function mod.ShouldSodomOrGomorrahMove(sodom, gomorrah, movement)
	local heart = sodom:GetData().sodomgomorrah_chainedheart
	local heartpos = heart and heart.Position or (sodom.Position + gomorrah.Position) / 2
	local trackpos = heartpos + movement:Resized(30)

	if mod.CanPlayerJump(sodom) and sodom.Position:Distance(trackpos) >= gomorrah.Position:Distance(trackpos) or not mod.CanPlayerJump(gomorrah) then
		return sodom, gomorrah
	elseif mod.CanPlayerJump(gomorrah) and sodom.Position:Distance(trackpos) < gomorrah.Position:Distance(trackpos) or not mod.CanPlayerJump(sodom) then
		return gomorrah, sodom
	else
		print("WARN: No player decided fit to jump, state should not have been reached")
		return sodom, gomorrah
	end
end

function mod.CanPlayerJump(player, tainted)
	local data = player:GetData()
	local other = data.sodom or data.gomorrah

	local prevent = player:IsHoldingItem()
					or (not player:IsExtraAnimationFinished() and not player:GetSprite():IsPlaying("Hit"))
					or data.jump_velocity
					or (other and (other:GetData().jump_velocity and not tainted))
					or (mod.GetOpenDSSMenu and mod.GetOpenDSSMenu())
					or data.stopactive
					or (tainted and data.landinglag ~= 0)
					or data.diewhenready

	return not prevent
end

function mod.CanSodomAndGomorrahMove(sodom, gomorrah)
	if not sodom or not gomorrah then
		if not mod.IsChallengePillarOfSalt() then
			return false
		end
	end

	if mod.IsChallengePillarOfSalt() then
		return mod.CanPlayerJump(sodom)
	else
		return mod.CanPlayerJump(sodom) or mod.CanPlayerJump(gomorrah)
	end
end

function mod.MakeHpTemplate(sodom, origin)
	origin = origin or sodom

	local template = {
		max		= {EntityPlayer.__class.GetMaxHearts,		origin:GetMaxHearts()},
		red		= {EntityPlayer.__class.GetHearts,			origin:GetHearts()},
		soul	= {EntityPlayer.__class.GetSoulHearts,		origin:GetSoulHearts()},
		black	= {EntityPlayer.__class.GetBlackHearts,		mod.CountBits(origin:GetBlackHearts()), mod.CountBits},
		golden	= {EntityPlayer.__class.GetGoldenHearts,	origin:GetGoldenHearts()},
		eternal = {EntityPlayer.__class.GetEternalHearts,	origin:GetEternalHearts()},
		bone	= {EntityPlayer.__class.GetBoneHearts,		origin:GetBoneHearts()},
		rotten	= {EntityPlayer.__class.GetRottenHearts,	origin:GetRottenHearts()},
		broken	= {EntityPlayer.__class.GetBrokenHearts,	origin:GetBrokenHearts()},
	}

	sodom:GetData().hp_template = template
	return template
end

function mod.IsHpTemplateWrong(template, sodom, gomorrah)
	local yes, also

	for _, player in pairs({sodom, gomorrah}) do
		for _, value in pairs(template) do
			local test = value[1](player)
			if value[3] then test = value[3](test) end

			if value[2] ~= test then
				yes = player
				break
			end
		end
		if yes then break end
	end

	if yes and (mod.IsPlayerSodom(yes) or (yes:GetPlayerType() == mod.PLAYER_TYPE.BELA and yes:GetData().gomorrah)) then
		also = gomorrah
	else
		also = sodom
	end

	return yes, also
end

function mod.CorrectHp(template, player)
	player:AddBrokenHearts(template.broken[2] - player:GetBrokenHearts())
	player:AddMaxHearts(template.max[2] - player:GetMaxHearts())
	player:AddBoneHearts(template.bone[2] - player:GetBoneHearts())

	player:AddRottenHearts(template.rotten[2] - player:GetRottenHearts())
	player:AddHearts(template.red[2] - player:GetHearts())

	player:AddBlackHearts(template.black[2] - mod.CountBits(player:GetBlackHearts()))
	player:AddSoulHearts(template.soul[2] - player:GetSoulHearts())

	player:AddGoldenHearts(template.golden[2] - player:GetGoldenHearts())
	player:AddEternalHearts(template.eternal[2] - player:GetEternalHearts())
end

function mod.CheckHpTemplate(sodom, gomorrah)
	local template = sodom:GetData().hp_template or mod.MakeHpTemplate(sodom)

	local violator, other = mod.IsHpTemplateWrong(template, sodom, gomorrah)

	if violator then
		mod.MakeHpTemplate(sodom, violator)
		mod.CorrectHp(sodom:GetData().hp_template, other)
	end
end

function mod.CheckSodomGomorrahGridCollision(player)
	if player.CanFly then return end

	local room = game:GetRoom()
	local x = player.Velocity.X
	local y = player.Velocity.Y

	local check = player.Position + Vector(x, 0):Resized(math.abs(x) + player.Size)
	local grid = room:GetGridEntityFromPos(check)

	if grid and mod.DoesSodomGomorrahCollideGrid(grid) then
		x = 0
	end

	check = player.Position + Vector(0, y):Resized(math.abs(x) + player.Size)
	grid = room:GetGridEntityFromPos(check)

	if grid and mod.DoesSodomGomorrahCollideGrid(grid) then
		y = 0
	end

	player.Velocity = Vector(x, y)
end

function mod.CheckFlight(player, key)
	local data = player:GetData()
	if data.flightcheck == nil then data.flightcheck = player.CanFly end

	if player.CanFly ~= data.flightcheck then
		mod.ChangePlayerAnm2(player, key)
	end

	data.flightcheck = player.CanFly
end

function mod.FixHeartbreakPickup(sodom)
	if not mod.AntiHeartbreakRecursion then
		local num = sodom:GetCollectibleNum(CollectibleType.COLLECTIBLE_HEARTBREAK)
		local data = sodom:GetData()

		data.heartbreaknum = data.heartbreaknum or num

		if data.heartbreaknum < num then
			mod.AntiHeartbreakRecursion = true

			local curr = data.storedbrokenhearts

			sodom:Update()

			local brokens = sodom:GetBrokenHearts()
			local diff = brokens - curr

			if diff > 3 then
				sodom:AddBrokenHearts(3 - diff)
			end

			mod.AntiHeartbreakRecursion = false
		end

		data.heartbreaknum = num
		data.storedbrokenhearts = sodom:GetBrokenHearts()
	end
end

function mod.FixBatteryPackPickup(gomorrah)
	local num = gomorrah:GetCollectibleNum(CollectibleType.COLLECTIBLE_BATTERY_PACK)
	local data = gomorrah:GetData()

	data.batterypacknum = data.batterypacknum or num

	if data.batterypacknum < num then
		local sodom = data.sodom
		sodom:FullCharge()
	end

	data.batterypacknum = num
end

function mod.FixBirthrightPickup(player)
	local has = player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT)
	local data = player:GetData()

	if data.sodomgomorrah_hasbirthright == nil then data.sodomgomorrah_hasbirthright = has end

	if data.sodomgomorrah_hasbirthright ~= has then
		player:AddCacheFlags(CacheFlag.CACHE_FAMILIARS)
		player:EvaluateItems()
	end

	data.sodomgomorrah_hasbirthright = has
end

function mod.ShouldEntityCollideWhileJumping(entity)
	if entity:ToNPC() then return false end
	if entity:ToProjectile() then return false end

	local pickup = entity:ToPickup()
	if pickup then
		if pickup.Variant == 100 or pickup.Variant == 110 or pickup.Variant == 300 or pickup.Variant == 350 or pickup.Variant == 70 or pickup.Variant == 41 or pickup.Price ~= 0 then
			return false
		else
			return true
		end
	end
end

function mod.ShouldEntityPassDamage(ref)
	return ref.Type == 6 and (1 << ref.Variant) & mod.SLOT_MACHINE_DAMAGE_VARIANT_MASK > 0
end

function mod.ShouldPositionTargetShow(owner)
	return  mod.savedata.position_indicator > 1 and
			owner:GetData().jump_velocity and
			not owner:IsDead()
end

function mod.TransferGomorrahActives(gomorrah, sodom)
	local active = gomorrah:GetActiveItem()
	local charge = gomorrah:GetActiveCharge()

	gomorrah:RemoveCollectible(active)

	if active ~= 0 then
		local s_active = sodom:GetActiveItem()
		local s_active2

		if sodom:HasCollectible(CollectibleType.COLLECTIBLE_SCHOOLBAG) then
			s_active2 = sodom:GetActiveItem(ActiveSlot.SLOT_SECONDARY)
		end

		local pedestal = mod.GetClosest(gomorrah.Position, 5, 100, 0)
		pedestal = pedestal and pedestal:ToPickup()

		local data = sodom:GetData()

		if s_active2 then
			if s_active2 ~= 0 then
				if pedestal then
					pedestal:Morph(5, 100, s_active, true, false, true)
					pedestal.Charge = sodom:GetActiveCharge()
					pedestal.Touched = true
				else
					local room = game:GetRoom()
					local new = Isaac.Spawn(5, 100, s_active, room:FindFreePickupSpawnPosition(gomorrah.Position), Vector.Zero, gomorrah):ToPickup()
					new.Charge = sodom:GetActiveCharge()
					new.Touched = true
				end
			end

			sodom:AddCollectible(active, charge, false)
		else
			if s_active ~= 0 then
				if pedestal then
					pedestal:Morph(5, 100, s_active, true, false, true)
					pedestal.Charge = sodom:GetActiveCharge()
					pedestal.Touched = true
				else
					local room = game:GetRoom()
					local new = Isaac.Spawn(5, 100, s_active, room:FindFreePickupSpawnPosition(gomorrah.Position), Vector.Zero, gomorrah):ToPickup()
					new.Charge = sodom:GetActiveCharge()
					new.Touched = true
				end
			end

			sodom:AddCollectible(active, charge, false)
		end

		gomorrah:AddCacheFlags(CacheFlag.CACHE_ALL)
		gomorrah:EvaluateItems()

		sodom:AddCacheFlags(CacheFlag.CACHE_ALL)
		sodom:EvaluateItems()
	end
end

function mod.TransferGomorrahPillsCards(gomorrah, sodom)
	local card = gomorrah:GetCard(0)
	local pill = gomorrah:GetPill(0)

	if card + pill > 0 then
		local slots = mod.CalculatePocketSize(sodom)
		local s_pockets = {
			sodom:GetCard(0) + sodom:GetPill(0),
			sodom:GetCard(1) + sodom:GetPill(1),
		}

		if s_pockets[slots] > 0 then
			sodom:DropPocketItem(0, game:GetRoom():FindFreePickupSpawnPosition(gomorrah.Position))
		end

		if card > 0 then
			sodom:AddCard(card)

			gomorrah:DropPocketItem(0, gomorrah.Position)
			local pickup = mod.GetClosest(gomorrah.Position, 5, 300)
			if pickup then pickup:Remove() end
		end

		if pill > 0 then
			sodom:AddPill(pill)

			gomorrah:DropPocketItem(0, gomorrah.Position)
			local pickup = mod.GetClosest(gomorrah.Position, 5, 70)
			if pickup then pickup:Remove() end
		end
	end
end

function mod.TransferGomorrahTrinkets(gomorrah, sodom)
	local trinket = gomorrah:GetTrinket(0)

	if trinket > 0 then
		local slots = mod.CalculateTrinketPocketSize(sodom)
		local s_trinkets = {
			sodom:GetTrinket(0),
			sodom:GetTrinket(1),
		}

		if s_trinkets[slots] > 0 then
			local choice = s_trinkets[1] == TrinketType.TRINKET_TICK and s_trinkets[2] or s_trinkets[1]

			sodom:TryRemoveTrinket(choice)
			Isaac.Spawn(5, 350, choice, game:GetRoom():FindFreePickupSpawnPosition(gomorrah.Position + RandomVector():Resized(40)), Vector.Zero, gomorrah):ToPickup().Wait = 30
		end

		sodom:AddTrinket(trinket)
		gomorrah:TryRemoveTrinket(trinket)
	end
end

function mod.TransferMainOnlyItemTypes(gomorrah, sodom)
	mod.TransferGomorrahActives(gomorrah, sodom)
	mod.TransferGomorrahPillsCards(gomorrah, sodom)
	mod.TransferGomorrahTrinkets(gomorrah, sodom)
end

function mod.TransferMainOnlyItems(gomorrah, sodom)
	for _, item in pairs(mod.SodomGomorrahMainOnlyItems) do
		if gomorrah:HasCollectible(item) then
			sodom:AddCollectible(item, 0, false)
			gomorrah:RemoveCollectible(item)
		end
	end
end

function mod.OnTakeDevilDeal(player, pickup)
	local data = player:GetData()
	local other = data.sodom or data.gomorrah

	local souls = 0
	local hearts = 0

	if pickup.Price == PickupPrice.PRICE_ONE_HEART then
		hearts = 2
	elseif pickup.Price == PickupPrice.PRICE_TWO_HEARTS then
		hearts = 4
	elseif pickup.Price == PickupPrice.PRICE_THREE_SOULHEARTS then
		souls = 6
	elseif pickup.Price == PickupPrice.PRICE_ONE_HEART_AND_TWO_SOULHEARTS then
		hearts = 2
		souls =  4
	end

	other:AddMaxHearts(-hearts)
	other:AddSoulHearts(-souls)

	if mod.CalcPlayerHealth(other) - other:GetEternalHearts() <= 0 then 
		other:GetData().diewhenready = true
	end
end

function mod.CheckHeartCostumes(sodom, gomorrah, heart, force)
	local data = heart:GetData()
	local sprite = heart:GetSprite()

	local sodom_effects = sodom:GetEffects()
	local gomorrah_effects = gomorrah:GetEffects()

	local mantle = sodom_effects:GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_HOLY_MANTLE) + gomorrah_effects:GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_HOLY_MANTLE)
	local shadows = sodom_effects:GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS) + gomorrah_effects:GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_BOOK_OF_SHADOWS)

	--local leaf = sodom_effects:HasNullEffect(NullItemID.ID_STATUE) or gomorrah_effects:HasNullEffect(NullItemID.ID_STATUE)
	--local leaf = sodom:GetData().isgnawedleaf or gomorrah:GetData().isgnawedleaf

	local leaf = sodom_effects:HasNullEffect(NullItemID.ID_REVERSE_CHARIOT) or gomorrah_effects:HasNullEffect(NullItemID.ID_REVERSE_CHARIOT)
	local tooth = mod.GetToothAndNailStatus(sodom) or mod.GetToothAndNailStatus(gomorrah)

	local shackle = sodom:HasCollectible(CollectibleType.COLLECTIBLE_SPIRIT_SHACKLES) and not sodom:GetEffects():HasNullEffect(NullItemID.ID_SPIRIT_SHACKLES_DISABLED)

	if mantle > 0 and not data.mantle then
		data.mantle = true

		sprite:ReplaceSpritesheet(2, mod.CHAINED_HEART_GFX_PATH.MANTLE_1)
		sprite:ReplaceSpritesheet(3, mod.CHAINED_HEART_GFX_PATH.MANTLE_2)
	elseif mantle == 0 and data.mantle then
		data.mantle = false

		sprite:ReplaceSpritesheet(2, mod.CHAINED_HEART_GFX_PATH.NULL)
		sprite:ReplaceSpritesheet(3, mod.CHAINED_HEART_GFX_PATH.NULL)
	end

	if shadows > 0 and not data.shadows then
		data.shadows = true

		sprite:ReplaceSpritesheet(4, mod.CHAINED_HEART_GFX_PATH.SHADOWS)
	elseif shadows == 0 and data.shadows then
		data.shadows = false

		sprite:ReplaceSpritesheet(4, mod.CHAINED_HEART_GFX_PATH.NULL)
	end

	local expected = "default"
	local expected2 = "default"

	if tooth then
		expected = "tooth_nail"
	elseif leaf then
		expected = "gnawed_leaf"
	end

	if shackle then
		expected2 = "blue_chains"
	end

	data.skin = data.skin or "default"
	data.skin2 = data.skin2 or "default"

	if data.skin ~= expected or data.skin2 ~= expected2 or force then
		if data.skin2 ~= expected2 or force then
			for _, chain in pairs(heart:GetData().sodom_chains) do
				local spr = chain:GetSprite()
				spr:ReplaceSpritesheet(1, mod.CHAINED_HEART_GFX_PATH[string.upper(expected2)])
				spr:LoadGraphics()
			end

			for _, chain in pairs(heart:GetData().gomorrah_chains) do
				local spr = chain:GetSprite()
				spr:ReplaceSpritesheet(1, mod.CHAINED_HEART_GFX_PATH[string.upper(expected2)])
				spr:LoadGraphics()
			end
		end

		data.skin = expected
		data.skin2 = expected2

		sprite:ReplaceSpritesheet(0, mod.CHAINED_HEART_GFX_PATH[string.upper(expected)])
		sprite:ReplaceSpritesheet(1, mod.CHAINED_HEART_GFX_PATH[string.upper(expected2)])
	end

	sprite:LoadGraphics()
end

function mod.AddInfamyTint(entity)
	entity:SetColor(mod.INFAMY_TINT, 15, 0, true, true)
end

function mod.CommonSodomGomorrahAI(player)
	local data = player:GetData()
	local sprite = player:GetSprite()
	local movement = player:GetMovementInput():Normalized()
	local other = data.sodom or data.gomorrah

	player:ClearCostumes()

	if data.diewhenready and not other:IsHoldingItem() then
		data.passdamage = true
		player:TakeDamage(1, 0, EntityRef(player), 0)
		data.diewhenready = false
	end

	if data.jump_velocity then
		if mod.savedata.position_indicator == 2 then
			data.position_indicator.Position = player.Position
		elseif mod.savedata.position_indicator == 3 then
			local frames = 0
			local jump_vel = data.jump_velocity
			local off = player.SpriteOffset.Y

			repeat
				off = off - player.MoveSpeed * jump_vel / 2
				jump_vel = jump_vel - player.MoveSpeed / 2

				frames = frames + 1
			until off - jump_vel > 0

			data.position_indicator.Position = mod.Lerp(data.position_indicator.Position, player.Position + player.Velocity * frames, 0.25)
		end

		mod.DoJump(player, movement)
	else
		player.Velocity = player.Velocity * 0.6

		data.position_indicator.Position = player.Position
	end

	mod.CheckGridSoftlockStatus(player)

	if sprite:IsPlaying("TeleportDown") then
		player.Velocity = Vector.Zero

		if not mod.IsChallengePillarOfSalt() then
			if sprite:GetFrame() == 11 and not other:GetSprite():IsPlaying("TeleportDown") then
				other:PlayExtraAnimation("TeleportDown")
				other.Position = player.Position

				for i = 1, 10 do other:GetSprite():Update() end
			end
		end
	end

	if mod.GetLostCurseStatus(player) and (not data.ghosts or not data.ghosts[1]:Exists()) then
		data.ghosts = {
			Isaac.Spawn(1000, mod.EFFECT.GHOSTS, 0, player.Position, Vector.Zero, player),
			Isaac.Spawn(1000, mod.EFFECT.GHOSTS, 1, player.Position, Vector.Zero, player),
		}

		for _, ghost in pairs(data.ghosts) do
			ghost:GetData().player = player
		end
	elseif data.ghosts and not mod.GetLostCurseStatus(player) then
		data.ghosts[1]:Remove()
		data.ghosts[2]:Remove()

		data.ghost = nil
	end

	if not mod.IsChallengePillarOfSalt() then
		mod.LostCurseParity(player, other)
		mod.SpiritShackleParity(player, other)
	end

	mod.CheckReplacerItems(player)
	mod.FixBirthrightPickup(player)

	if player:HasCollectible(CollectibleType.COLLECTIBLE_TOOTH_AND_NAIL) and mod.DidToothAndNailJustChange(player) then
		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end

	if player:IsDead() then
		if not mod.IsChallengePillarOfSalt() then
			if not other:IsDead() then
				other:Die()
			end
		end
	else
		if data.stopactive and not player.jump_velocity then
			player.Velocity = Vector.Zero
			if player:IsExtraAnimationFinished() then
				sprite:Play("WalkDown")

				if player:GetShootingJoystick():Length() == 0 then
					sprite:PlayOverlay("HeadDown")
				end
			end
		end
	end

	data.lastvel = player.Velocity
end

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, function(_, player)
	if mod.Clickering then return end

	if mod.IsPlayerSodomGomorrah(player) then
		if mod.GlowingHourglass == false then
			mod.GlowingHourglass = nil
			mod.AssignPlayerHUDs()
		end
	end

	local typ = player:GetPlayerType()
	if typ == mod.PLAYER_TYPE.SODOM or typ == mod.PLAYER_TYPE.SODOM2 then
		local data = player:GetData()
		local movement = player:GetMovementInput():Normalized()

		if not mod.IsChallengePillarOfSalt() then
			if data.gomorrah then
				if data.nohpcheck then
					data.nohpcheck = nil
					mod.MakeHpTemplate(player)
				else
					mod.CheckHpTemplate(player, data.gomorrah)
				end

				if mod.CanSodomAndGomorrahMove(player, data.gomorrah) and movement:Length() > 0 and player.FrameCount > 0 then
					mod.CheckMover(player, movement)
				end

				if not data.jump_velocity and not mod.IsChallengePillarOfSalt() then
					mod.LimitStationaryChainLength(player, data.gomorrah)
				end

				if data.gomorrah_pillcard and Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex) then
					local card = player:GetCard(0)
					local pill = player:GetPill(0)

					player:DropPocketItem(0, player.Position)

					if card > 0 then
						data.gomorrah:UseCard(card)

						local pickup = mod.GetClosest(player.Position, 5, 300)
						if pickup then pickup:Remove() end
					elseif pill > 0 then
						local effect = game:GetItemPool():GetPillEffect(pill, data.gomorrah)
						data.gomorrah:UsePill(effect, pill)

						local pickup = mod.GetClosest(player.Position, 5, 70)
						if pickup then pickup:Remove() end
					end

					data.gomorrah_pillcard = false
				end

				if data.sodomgomorrah_chainedheart then
					mod.CheckHeartCostumes(player, data.gomorrah, data.sodomgomorrah_chainedheart)
				end

				if mod.IsChallengeCardiacArrest() then
					if not data.challenge_chains or not data.challenge_chains[1]:Exists() then
						data.challenge_chains = {}

						for i = 1, 9 do
							data.challenge_chains[i] = Isaac.Spawn(1000, mod.EFFECT.CHAINED_HEART_CHAIN, 0, player.Position, Vector.Zero, player)
						end
					end

					for index, link in pairs(data.challenge_chains) do
						link.SpriteScale = player.SpriteScale + (data.gomorrah.SpriteScale - player.SpriteScale) * index/9

						if player.FrameCount == 0 then
							link.Position = (player.Position - data.gomorrah.Position) * index / 9 + data.gomorrah.Position + Vector(0, -0.1)
						else
							link.Velocity = (player.Position - data.gomorrah.Position) * index / 9 + data.gomorrah.Position - link.Position
						end
					end

					if player.SpriteOffset.Y >= data.gomorrah.SpriteOffset.Y then
						for index, link in pairs(data.challenge_chains) do
							link.SpriteOffset = player.SpriteOffset + Vector(0, -math.abs(player.SpriteOffset.Y - data.gomorrah.SpriteOffset.Y)) * (9 - index) / 9
						end
					else
						for index, link in pairs(data.challenge_chains) do
							link.SpriteOffset = data.gomorrah.SpriteOffset + Vector(0, -math.abs(data.gomorrah.SpriteOffset.Y - player.SpriteOffset.Y)) * index / 9
						end
					end
				end

				mod.RatifyRevivers(player, data.gomorrah)
				mod.FixHeartbreakPickup(player)
			else
				player:SetPocketActiveItem(mod.ITEM.STOP, ActiveSlot.SLOT_POCKET, false)
				mod.ChangePlayerAnm2(player, "sodom")

				data.position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, player.Position, Vector.Zero, player)
				data.position_indicator.Visible = false
				data.position_indicator:GetSprite():Play("Sodom")

				mod.SummonGomorrah(player)
			end
		else
			if movement:Length() > 0 and mod.CanPlayerJump(player) then
				mod.CheckMover(player, movement, true)
			end

			if not data.position_indicator then
				mod.ChangePlayerAnm2(player, "sodom")

				data.position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, player.Position, Vector.Zero, player)
				data.position_indicator.Visible = false
				data.position_indicator:GetSprite():Play("Sodom")
			end
		end

		mod.CheckFlight(player, "sodom")
		mod.CommonSodomGomorrahAI(player)
	elseif typ == mod.PLAYER_TYPE.GOMORRAH then
		local data = player:GetData()

		if data.sodom then
			player.ControlsEnabled = data.sodom.ControlsEnabled

			local ticks = (data.sodom:GetTrinket(0) == TrinketType.TRINKET_TICK and 1 or 0) + (data.sodom:GetTrinket(1) == TrinketType.TRINKET_TICK and 1 or 0)
			data.canpicktrinkets = ticks ~= mod.CalculateTrinketPocketSize(data.sodom)

			if not player:IsDead() then
				mod.TransferMainOnlyItems(player, data.sodom)
				mod.TransferMainOnlyItemTypes(player, data.sodom)

				mod.FixBatteryPackPickup(player)
			end

			if Input.IsActionTriggered(ButtonAction.ACTION_BOMB, player.ControllerIndex) and not player:HasGoldenBomb() then
				local valid = 0
				for _, bomb in pairs(Isaac.FindByType(4)) do
					if bomb.FrameCount < 1 then
						valid = valid + 1
					end
				end

				if valid > 1 then
					player:AddBombs(1)
				elseif valid == 1 then
					Isaac.Spawn(4, 0, 0, player.Position, Vector.Zero, player):ToBomb().Flags = player:GetBombFlags()
				end
			end
		end

		mod.CheckFlight(player, "gomorrah")
		mod.CommonSodomGomorrahAI(player)
	elseif typ ~= mod.PLAYER_TYPE.SODOM_B and typ ~= mod.PLAYER_TYPE.BELA then
		if player:GetData().gomorrah and player:GetData().gomorrah:ToPlayer() then
			mod.AssassinateGomorrah(player, true)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	if effect.SpawnerEntity then
		effect:AddEntityFlags(EntityFlag.FLAG_PERSISTENT)
		effect.Visible = mod.ShouldPositionTargetShow(effect.SpawnerEntity)
	else
		effect:Remove()
	end
end, mod.EFFECT.POSITION_TARGET)

mod:AddPriorityCallback(ModCallbacks.MC_PRE_PLAYER_COLLISION, CallbackPriority.IMPORTANT, function(_, player, collider)
	local typ = player:GetPlayerType()
	local data = player:GetData()

	if collider.Type == 5 and collider.Variant == 41 and mod.IsPlayerSodomGomorrah(player, false, true) then
		data.RedBombCollision = true
	end

	if collider.Type == 33 and collider.Variant == 4 then -- White Fire Place
		if mod.IsPlayerSodomGomorrah(player, false, true) then
			if #Isaac.FindByType(1000, mod.EFFECT.BELA) > 0 then
				return true
			else
				return nil
			end
		end
	end

	if mod.IsPlayerSodomGomorrah(player) then
		if data.jump_velocity and not mod.ShouldEntityCollideWhileJumping(collider) then
			return true
		end
	end

	if (collider:ToNPC() or collider:ToProjectile()) and player:GetEffects():GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_HOLY_MANTLE) > 0 and not data.jump_velocity then
		if mod.IsPlayerSodomGomorrah(player, true) then
			return false
		elseif mod.IsPlayerSodomGomorrah(player, false, true) then
			if not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
				return false
			end
		end
	end

	if typ == mod.PLAYER_TYPE.GOMORRAH then
		if collider.Type == 5 and collider.Variant == 350 and not player:GetData().canpicktrinkets then
			return false
		end
	end

	if player.Variant == 0 and collider.Type == 6 and collider.Variant == mod.LOCKED_SODOM then
		local sprite = collider:GetSprite()
		if sprite:GetAnimation() == "Idle" then
			sprite:Play("PayPrize")
		end
	end
end)

mod:AddPriorityCallback(ModCallbacks.MC_PRE_NPC_COLLISION, CallbackPriority.IMPORTANT, function(_, npc, collider)
	if collider.Type == 1 then
		local player = collider:ToPlayer()

		if npc.Type == 33 and npc.Variant == 4 then -- White Fire Place
			if mod.IsPlayerSodomGomorrah(player, false, true) then
				if #Isaac.FindByType(1000, mod.EFFECT.BELA) > 0 then
					return true
				else
					return nil
				end
			elseif mod.IsPlayerSodomGomorrah(player, true) then
				return nil
			end
		end

		if player:GetData().jump_velocity then
			if mod.IsPlayerSodomGomorrah(player) then
				return true
			end
		else
			if mod.IsPlayerSodomGomorrah(player, true) then
				return false
			elseif mod.IsPlayerSodomGomorrah(player, false, true) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
				return false
			end
		end
	end
end)

mod:AddPriorityCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, CallbackPriority.IMPORTANT, function(_, pickup, collider)
	local player = collider:ToPlayer()

	if player and (mod.IsPlayerSodomGomorrah(player) or player:GetPlayerType() == mod.PLAYER_TYPE.BELA) then
		local data = player:GetData()

		if mod.IsPlayerSodomGomorrah(player) then
			if data.jump_velocity and not mod.ShouldEntityCollideWhileJumping(pickup) then
				return true
			end

			if pickup.Price ~= 0 and pickup.FrameCount < 45 then
				return true
			end

			if pickup.Variant == 350 and data.canpicktrinkets ~= nil and not data.canpicktrinkets then
				return false
			end
		end

		if not mod.IsChallengePillarOfSalt() then
			if (pickup.Variant == PickupVariant.PICKUP_LIL_BATTERY or (pickup.Variant == PickupVariant.PICKUP_KEY and pickup.SubType == KeySubType.KEY_CHARGED)) and (mod.IsPlayerGomorrah(player) or (player:GetPlayerType() == mod.PLAYER_TYPE.BELA and player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and data.sodom)) then
				local sodom = data.sodom
				if mod.CanPlayerPickBattery(sodom) then
					if pickup.Price > 0 or pickup.Price == PickupPrice.PRICE_FREE then
						if pickup.Price <= player:GetNumCoins() then
							local pos = pickup.Position
							local coll = sodom.EntityCollisionClass

							mod.TimeTravelling = true

							sodom.EntityCollisionClass = 4
							pickup.Position = sodom.Position
							
							game:Update()
							
							sodom.EntityCollisionClass = coll
							pickup.Position = pos

							game:Update()

							mod.TimeTravelling = false
						end
					else
						local pos = pickup.Position
						local coll = sodom.EntityCollisionClass

						mod.TimeTravelling = true

						sodom.EntityCollisionClass = 4
						pickup.Position = sodom.Position
						
						game:Update()
						
						sodom.EntityCollisionClass = coll
						pickup.Position = pos

						mod.TimeTravelling = false
					end

					return true
				end
			end
		end

		if not player:IsHoldingItem() and pickup.Price < 0 and pickup.Price >= PickupPrice.PRICE_ONE_HEART_AND_TWO_SOULHEARTS then
			mod.OnTakeDevilDeal(player, pickup)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	if pickup:GetData().sodomgomorrah_remove and pickup.SubType ~= 0 then
		pickup.Touched = true
		pickup.SubType = 0

		local sprite = pickup:GetSprite()
		sprite:ReplaceSpritesheet(1, "nonsense.png")
		sprite:ReplaceSpritesheet(4, "nonsense.png")
		sprite:LoadGraphics()
	end
end, 100)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	for i = 0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(i)

		if mod.IsPlayerSodomGomorrah(player) then
			local sprite = player:GetSprite()
			sprite:Play("WalkDown")
			sprite:Play("HeadDown")

			local data = player:GetData()
			data.stopactive = false

			mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))

			if mod.DidDimensionJustChange() then
				player:GetData().lastvel = Vector.Zero
			end

			player:GetData().softlock_avoiding = false
			player:AddCacheFlags(CacheFlag.CACHE_FLYING)
			player:EvaluateItems()
		end
	end

	local player = Isaac.GetPlayer(0)
	if player:GetPlayerType() == mod.PLAYER_TYPE.SODOM then
		local level = game:GetLevel()

		if level:GetStage() == LevelStage.STAGE8 and level:GetCurrentRoomDesc().SafeGridIndex == 94 then
			if not mod.savedata.unlocked_tainted then
				for _, shopkeeper in pairs(Isaac.FindByType(17)) do
					shopkeeper:Remove()

					Isaac.Spawn(6, mod.LOCKED_SODOM, 0, shopkeeper.Position, Vector.Zero, nil)
				end

				for _, item in pairs(Isaac.FindByType(5, 100)) do
					item:Remove()

					Isaac.Spawn(6, mod.LOCKED_SODOM, 0, item.Position, Vector.Zero, nil)
				end
			end
		end
	end

	if mod.GlowingHourglass then
		mod.GlowingHourglass = false
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	for _, slot in pairs(Isaac.FindByType(6, mod.LOCKED_SODOM)) do
		local sprite = slot:GetSprite()

		if slot.GridCollisionClass == 5 then
			for _, pickup in pairs(Isaac.FindByType(5)) do
				if pickup.FrameCount == 2 then
					pickup:Remove()
				end
			end

			sprite:Play("PayPrize")

			slot.Velocity = slot.Velocity * 0.9
		end

		if sprite:IsEventTriggered("FX") then
			for i = 1, 5 do
				Isaac.Spawn(1000, 193, 0, slot.Position, Vector.Zero, nil)
			end
		end

		if sprite:IsEventTriggered("Poof") then
			sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 1, 0, false, 0.7)
		end

		if sprite:IsFinished("PayPrize") then
			if game.Challenge == 0 then
				mod.QueueAchievementNote("gfx/ui/achievement/sodom_gomorrah_b.png")

				mod.savedata.unlocked_tainted = true
				mod.SaveSaveData()
			end
			slot:Remove()
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player, flag)
	local typ = player:GetPlayerType()

	if typ == mod.PLAYER_TYPE.SODOM or typ == mod.PLAYER_TYPE.SODOM2 then
		if not mod.IsChallengePillarOfSalt() and not mod.IsChallengeCardiacArrest() then
			for _, link in pairs(Isaac.FindByType(1000, mod.EFFECT.CHAINED_HEART_CHAIN)) do
				link:Update()
			end

			player:CheckFamiliar(mod.FAMILIAR.CHAINED_HEART, 1, RNG(), nil, 179)
		end

		player:CheckFamiliar(mod.FAMILIAR.BELA, player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and 1 or 0, RNG())
	elseif typ == mod.PLAYER_TYPE.GOMORRAH then
		local data = player:GetData()

		if data.sodom then
			data.sodom:CheckFamiliar(mod.FAMILIAR.ADMAH, player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and 1 or 0, RNG())
			data.sodom:CheckFamiliar(mod.FAMILIAR.ZEBOIM, player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and 1 or 0, RNG())
		end
	end
end, CacheFlag.FLAG_FAMILIARS)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player, flag)
	if mod.IsPlayerSodom(player) then
		if not mod.IsChallengePillarOfSalt() then
			local gomorrah = player:GetData().gomorrah

			if gomorrah then
				gomorrah:AddCacheFlags(CacheFlag.CACHE_FLYING)
				gomorrah:EvaluateItems()
			end
		end
	elseif mod.IsPlayerGomorrah(player) then
		local sodom = player:GetData().sodom

		if sodom and (sodom:HasCollectible(CollectibleType.COLLECTIBLE_PONY) or sodom:HasCollectible(CollectibleType.COLLECTIBLE_WHITE_PONY)) then
			if sodom.CanFly then
				player.CanFly = true
			end
		end
	end

	if mod.IsPlayerSodomGomorrah(player) then
		if player:GetData().softlock_avoiding then
			player.CanFly = true
		end

		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end
end, CacheFlag.CACHE_FLYING)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player, flag)
	local typ = player:GetPlayerType()

	if typ == mod.PLAYER_TYPE.SODOM or typ == mod.PLAYER_TYPE.SODOM2 then
		player.MaxFireDelay = player.MaxFireDelay * 0.85
	end
end, CacheFlag.CACHE_FIREDELAY)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player, flag)
	if not mod.IsChallengePillarOfSalt() then
		if mod.IsPlayerSodomGomorrah(player, true) and mod.savedata and mod.savedata.speed_mode == 2 then
			local data = player:GetData()
			local other = data.sodom or data.gomorrah

			if other then
				if data.flub_speed then
					player.MoveSpeed = other.MoveSpeed
					data.flub_speed = false
				else
					local data2 = other:GetData()

					data.basespeed = player.MoveSpeed
					data2.basespeed = data2.basespeed or other.MoveSpeed

					player.MoveSpeed = 1 + ((data.basespeed - 1) + (data2.basespeed - 1)) / 2

					data2.flub_speed = true
					other:AddCacheFlags(CacheFlag.CACHE_SPEED)
					other:EvaluateItems()
				end
			end
		end
	end
end, CacheFlag.CACHE_SPEED)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, function(_, familiar)
	if familiar.SubType == 179 then
		familiar:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	end
end, mod.FAMILIAR.CHAINED_HEART)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	if familiar.SubType == 179 then
		familiar:RemoveFromFollowers()

		local sodom = familiar.Player
		local sodom_data = familiar.Player:GetData()

		local gomorrah = sodom:GetData().gomorrah
		if not gomorrah then return end

		sodom_data.sodomgomorrah_chainedheart = familiar

		local data = familiar:GetData()
		local targetpos = (sodom.Position + gomorrah.Position) / 2

		if familiar.FrameCount == 0 then
			familiar.Position = targetpos
			familiar.Velocity = Vector.Zero
		else
			familiar.Velocity = targetpos - familiar.Position
		end
		--familiar.SpriteOffset = (sodom.SpriteOffset + gomorrah.SpriteOffset) / 2
		familiar.SpriteScale = (sodom.SpriteScale + gomorrah.SpriteScale) / 2

		familiar.EntityCollisionClass = familiar.SpriteOffset.Y <= -17 and 0 or 4


		local upgraded = sodom:HasCollectible(mod.ITEM.SODOMS_HEART) or gomorrah:HasCollectible(mod.ITEM.SODOMS_HEART)
		local jumping = sodom_data.jump_velocity or gomorrah:GetData().jump_velocity

		if not upgraded or jumping then
			familiar.FireCooldown = 0
			familiar:GetSprite():Play("Float")
		end
		
		if not data.sodom_chains or not data.sodom_chains[1]:Exists() then
			data.sodom_chains = {}

			for i = 1, 4 do
				data.sodom_chains[i] = Isaac.Spawn(1000, mod.EFFECT.CHAINED_HEART_CHAIN, 0, familiar.Position, Vector.Zero, familiar)
			end
		end

		if not data.gomorrah_chains or not data.gomorrah_chains[1]:Exists() then
			data.gomorrah_chains = {}

			for i = 1, 4 do
				data.gomorrah_chains[i] = Isaac.Spawn(1000, mod.EFFECT.CHAINED_HEART_CHAIN, 0, familiar.Position, Vector.Zero, familiar)
			end

			mod.CheckHeartCostumes(sodom, gomorrah, familiar, true)
		end

		local s2 = sodom.SpriteScale - familiar.SpriteScale
		local g2 = gomorrah.SpriteScale - familiar.SpriteScale

		for index, link in pairs(data.sodom_chains) do
			link.SpriteScale = familiar.SpriteScale + s2 * index / 5

			if familiar.FrameCount == 0 then
				link.Position = (sodom.Position - targetpos) * index / 5 + targetpos + Vector(0, -0.1)
			else
				link.Velocity = (sodom.Position - targetpos) * index / 5 + targetpos - link.Position
			end
		end

		for index, link in pairs(data.gomorrah_chains) do
			link.SpriteScale = familiar.SpriteScale + g2 * index / 5

			if familiar.FrameCount == 0 then
				link.Position = (gomorrah.Position - targetpos) * index / 5 + targetpos + Vector(0, -0.1)
			else
				link.Velocity = (gomorrah.Position - targetpos) * index / 5 + targetpos - link.Position
			end
		end

		local s = Vector(0, -math.abs(sodom.SpriteOffset.Y - familiar.SpriteOffset.Y))
		local g = Vector(0, -math.abs(gomorrah.SpriteOffset.Y - familiar.SpriteOffset.Y))

		if sodom.SpriteOffset.Y <= gomorrah.SpriteOffset.Y then
			for index, link in pairs(data.sodom_chains) do
				link.SpriteOffset = familiar.SpriteOffset + s * index / 5
			end

			for index, link in pairs(data.gomorrah_chains) do
				link.SpriteOffset = gomorrah.SpriteOffset + g * (5 - index) / 5
			end
		else
			for index, link in pairs(data.sodom_chains) do
				link.SpriteOffset = sodom.SpriteOffset + s * (5 - index) / 5
			end

			for index, link in pairs(data.gomorrah_chains) do
				link.SpriteOffset = familiar.SpriteOffset + g * index / 5
			end
		end
	end
end, mod.FAMILIAR.CHAINED_HEART)

mod:AddPriorityCallback(ModCallbacks.MC_PRE_FAMILIAR_COLLISION, CallbackPriority.IMPORTANT, function(_, familiar, collider, first)
	if familiar.SubType == 179 and collider:ToProjectile() and mod.SodomGomorrahHasInfamy(familiar) and not collider:GetData().nocheckchainedheart then
		local rng = familiar.Player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_INFAMY)
		local roll = rng:RandomInt(100)

		if roll < 50 then
			collider:GetData().passcollision = true
			collider:Die()

			mod.AddInfamyTint(familiar)
			return true
		else
			collider:GetData().nocheckchainedheart = true
		end
	end
end, mod.FAMILIAR.CHAINED_HEART)

mod:AddPriorityCallback(ModCallbacks.MC_PRE_PROJECTILE_COLLISION, CallbackPriority.IMPORTANT, function(_, projectile, collider, first)
	if collider:ToFamiliar() and collider.Variant == mod.FAMILIAR.CHAINED_HEART then
		if projectile:GetData().passcollision then
			return true
		end
	elseif collider.Type == 1 then
		local player = collider:ToPlayer()

		if mod.IsPlayerSodomGomorrah(player, true) and player:GetEffects():GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_HOLY_MANTLE) > 0 and not player:GetData().jump_velocity then
			return false
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	familiar:GetSprite():Play("Float")

	local data = familiar:GetData()
	if not data.chain or not data.chain:Exists() then
		data.chain = Isaac.Spawn(1000, mod.EFFECT.CHAINED_HEART_CHAIN, 0, familiar.Position, Vector.Zero, familiar)
	end

	data.angle = data.angle or math.random(360)

	local target = familiar.Player:GetData().sodomgomorrah_chainedheart or familiar.Player
	local targetpos = target.Position + Vector.FromAngle(data.angle):Resized(35)
	familiar.Velocity = targetpos - familiar.Position

	local joystick = familiar.Player:GetShootingJoystick()
	if joystick:Length() > 0 then
		local targetangle = joystick:GetAngleDegrees()

		if math.abs(data.angle - targetangle) > math.abs(data.angle + 360 - targetangle) then
			data.angle = data.angle + 360
		elseif math.abs(data.angle - targetangle) > math.abs(data.angle - 360 - targetangle) then
			data.angle = data.angle - 360
		end

		data.angle = mod.Lerp(data.angle, targetangle, 0.2)
	end

	data.chain.Velocity = (familiar.Position + target.Position) / 2 - data.chain.Position
	data.chain.SpriteOffset = familiar.SpriteOffset
end, mod.FAMILIAR.BELA)

mod:AddCallback(ModCallbacks.MC_PRE_FAMILIAR_COLLISION, function(_, familiar, collider)
	if collider:ToProjectile() then
		mod.AddInfamyTint(familiar)
		collider:Die()
	end
end, mod.FAMILIAR.BELA)

for _, variant in pairs({mod.FAMILIAR.ADMAH, mod.FAMILIAR.ZEBOIM}) do
	mod:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, function(_, familiar)
		familiar:AddToOrbit(mod.FAMILIAR.ADMAH)
		familiar:GetSprite():Play("Float")
	end, variant)

	mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
		familiar.OrbitDistance = Vector(20, 20)
		familiar.OrbitSpeed = -0.05

		local target = familiar.Player:GetData().sodomgomorrah_chainedheart or familiar.Player:GetData().gomorrah or familiar.Player
		familiar.Velocity = familiar:GetOrbitPosition(target.Position + target.Velocity) - familiar.Position
	end, variant)

	mod:AddCallback(ModCallbacks.MC_PRE_FAMILIAR_COLLISION, function(_, familiar, collider)
		if collider:ToProjectile() then
			mod.AddInfamyTint(familiar)
			collider:Die()
		end
	end, variant)
end

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect) -- I have literally no idea how we broke this but this hides them again
	effect.Visible = false
end, EffectVariant.FISSURE_SPAWNER)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect) -- Same with this
	effect.Visible = false
end, EffectVariant.SPAWNER)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_INIT, function(_, effect)
	effect:GetSprite():Play("Chain")
end, mod.EFFECT.CHAINED_HEART_CHAIN)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_INIT, function(_, effect)
	local data = effect:GetData()
	data.offset = Vector(0, -2 * effect.SubType + 1)

	if effect.SubType == 1 then
		local sprite = effect:GetSprite()
		sprite:ReplaceSpritesheet(0, "gfx/effects/sodom_ghosts_back.png")
		sprite:ReplaceSpritesheet(1, "gfx/effects/sodom_ghosts_back.png")
		sprite:LoadGraphics()
	end
end, mod.EFFECT.GHOSTS)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	local sprite = effect:GetSprite()
	local data = effect:GetData()
	local player = data.player
	if not player then return end

	local player_data = player:GetData()

	if sprite:IsFinished("throw") then
		sprite:Play("idle2")
	elseif sprite:IsFinished("catch") then
		sprite:Play("idle")
	end

	if player_data.jump_velocity and not data.jump_velocity then
		sprite:Play("throw")
	elseif data.jump_velocity and not player_data.jump_velocity then
		sprite:Play("catch")
	end
	data.jump_velocity = player_data.jump_velocity

	effect.Velocity = (player.Position + data.offset + player.Velocity) - effect.Position

	if player:IsDead() then
		effect.Color = Color(1, 1, 1, effect.Color.A * 0.9)
	else
		effect.Color = Color.Default
	end
end, mod.EFFECT.GHOSTS)

mod:AddPriorityCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, CallbackPriority.IMPORTANT, function(_, entity, amount, flags, source, cooldown)
	local player = entity:ToPlayer()
	local data = player:GetData()

	if not mod.IsChallengePillarOfSalt() then
		if mod.IsPlayerSodomGomorrah(player) then
			if source.Type == EntityType.ENTITY_FAMILIAR and source.Variant == FamiliarVariant.BLOOD_OATH then
				return
			end
		end

		if player:GetPlayerType() == mod.PLAYER_TYPE.SODOM then
			if data.passdamage then return end

			if mod.DamageIsSacrifice(flags) or mod.DamageIsMausDoor(flags, source) then
				data.gomorrah:GetData().passdamage = true
				data.gomorrah:TakeDamage(amount, 0, source, cooldown)
				data.gomorrah:GetData().passdamage = false
				return
			end

			if flags & mod.SODOM_GOMORRAH_DAMAGE_FLAG_MASK == 0 and not mod.DamageIsSacrifice(flags) and not mod.ShouldEntityPassDamage(source) then
				return false
			end

			if flags & DamageFlag.DAMAGE_CLONES == 0 then
				data.nohpcheck = true
				data.gomorrah:TakeDamage(amount, flags | DamageFlag.DAMAGE_CLONES, source, cooldown)

				return false
			elseif amount >= mod.CalcPlayerHealth(player) then
				if mod.HasCard(player, Card.CARD_SOUL_LAZARUS) then
					data.gomorrah:AddCard(Card.CARD_SOUL_LAZARUS)
				end
			end
		elseif player:GetPlayerType() == mod.PLAYER_TYPE.GOMORRAH then
			if data.passdamage then return end

			if mod.DamageIsSacrifice(flags) or mod.DamageIsMausDoor(flags, source) then
				data.sodom:GetData().passdamage = true
				data.sodom:TakeDamage(amount, 0, source, cooldown)
				data.sodom:GetData().passdamage = false
				return
			end

			if flags & DamageFlag.DAMAGE_CLONES == 0 then
				if mod.ShouldEntityPassDamage(source) or flags & mod.SODOM_GOMORRAH_DAMAGE_FLAG_MASK > 0 then
					data.sodom:TakeDamage(amount, flags, source, cooldown)
				end

				return false
			else
				data.sodom:TakeDamage(amount, flags, source, cooldown)
			end
		end
	end

	--[[if mod.IsPlayerSodomGomorrah(player, true) then
		if not mod.CurrentFloor then mod.ResetStageTracker() end

		mod.CurrentFloor.Damaged = true
	end]]
end, 1)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action <= ButtonAction.ACTION_DOWN then
		local player = entity and entity:ToPlayer()
		if not player then return end
		local data = player:GetData()

		if mod.IsPlayerSodomGomorrah(player, true) and not mod.CanSodomAndGomorrahMove(player, data.sodom or data.gomorrah) and not data.jump_velocity then
			return 0
		end
	elseif action >= ButtonAction.ACTION_SHOOTLEFT and action <= ButtonAction.ACTION_SHOOTDOWN then
		local player = entity and entity:ToPlayer()
		if not player then return end

		local data = player:GetData()
		if mod.IsPlayerSodomGomorrah(player, true) and data.jump_velocity then
			return 0
		end
	end
end, InputHook.GET_ACTION_VALUE)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action >= ButtonAction.ACTION_SHOOTLEFT and action <= ButtonAction.ACTION_SHOOTDOWN then
		local player = entity and entity:ToPlayer()
		if not player then return end

		local data = player:GetData()
		if mod.IsPlayerSodomGomorrah(player, true) and data.jump_velocity then
			return false
		end
	end
end, InputHook.IS_ACTION_PRESSED)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action == ButtonAction.ACTION_PILLCARD then
		local player = entity and entity:ToPlayer()
		if not player then return end

		if mod.IsPlayerSodom(player) and Input.IsActionPressed(ButtonAction.ACTION_DROP, player.ControllerIndex) and mod.ShouldGomorrahTryPillCard(player) then
			player:GetData().gomorrah_pillcard = true
			return false
		else
			player:GetData().gomorrah_pillcard = false
		end
	end
end, InputHook.IS_ACTION_TRIGGERED)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	if not effect.SpawnerEntity or not effect.SpawnerEntity:Exists() then
		effect:Remove()
	else
		effect.Visible = effect.SpawnerEntity.Visible
	end
end, mod.EFFECT.CHAINED_HEART_CHAIN)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, item, rng, player, flags, slot)
	if mod.IsPlayerSodom(player) then
		player:GetData().gomorrah:PlayExtraAnimation("TeleportUp")
	end
end, CollectibleType.COLLECTIBLE_STITCHES)

mod:AddCallback(ModCallbacks.MC_USE_PILL, function(_, effect, player)
	if mod.IsPlayerGomorrah(player) then
		player:AddCacheFlags(CacheFlag.CACHE_ALL)
		player:EvaluateItems()
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	if effect.SubType == 11 and effect.SpawnerEntity and effect.SpawnerEntity:ToPlayer() then -- Mantle Poof
		if mod.IsPlayerSodomGomorrah(effect.SpawnerEntity:ToPlayer(), true) then
			local sodom = effect.SpawnerEntity:GetData().sodom or effect.SpawnerEntity
			local heart = sodom:GetData().sodomgomorrah_chainedheart

			effect.Position = heart.Position

			effect.Parent = heart
			effect:FollowParent(heart)

			effect.SpriteOffset = heart.SpriteOffset + Vector(0, -5)
		end
	end
end, EffectVariant.POOF02)

mod:AddCallback(ModCallbacks.MC_POST_FAMILIAR_RENDER, function(_, familiar)
	if mod.IsPlayerSodomGomorrah(familiar.Player) or familiar.Player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
		if familiar.Variant == FamiliarVariant.ISAACS_HEART then
			familiar.SpriteOffset = mod.Lerp(familiar.SpriteOffset, familiar.Player.SpriteOffset, 0.15)

			if familiar.SpriteOffset.Y < -5 then
				familiar.EntityCollisionClass = 0
			else
				familiar.EntityCollisionClass = 4
			end
		end

		if familiar.OrbitDistance:Length() > 0 and familiar.Variant ~= FamiliarVariant.BLUE_SPIDER and familiar.Variant ~= FamiliarVariant.DIP then
			familiar.SpriteOffset = familiar.Player.SpriteOffset

			if familiar.SpriteOffset.Y < 0 then
				familiar.EntityCollisionClass = 0
			else
				familiar.EntityCollisionClass = 4
			end
		elseif familiar.Variant == 202 then
			familiar.SpriteOffset = familiar.Player.SpriteOffset
		elseif familiar.Variant == mod.FAMILIAR.CHAINED_HEART and familiar.SubType == 179 then
			familiar.SpriteOffset = (familiar.Player.SpriteOffset + familiar.Player:GetData().gomorrah.SpriteOffset) / 2
		end
	end

	if familiar.Variant == mod.FAMILIAR.BELA or familiar.Variant == mod.FAMILIAR.ADMAH or familiar.Variant == mod.FAMILIAR.ZEBOIM then
		local target = familiar.Player:GetData().sodomgomorrah_chainedheart
		if familiar.Variant == mod.FAMILIAR.BELA then
			target = target or familiar.Player
		else
			target = target or familiar.Player:GetData().gomorrah or familiar.Player
		end

		familiar.SpriteOffset = target.SpriteOffset
	end
end)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player, flags)
	if mod.IsPlayerSodom(player) then
		player:GetData().gomorrah:UseCard(Card.CARD_SOUL_LAZARUS, flags)
	end
end, Card.CARD_SOUL_LAZARUS)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, item, rng, player, flags, slot, custom)
	if mod.IsPlayerSodom(player) then
		if mod.ShouldGomorrahForceUseItem(item) then
			player:GetData().gomorrah:UseActiveItem(item, flags, slot)
		elseif Input.IsActionPressed(ButtonAction.ACTION_DROP, player.ControllerIndex) and mod.CanGomorrahUseItem(item) then
			local gomorrah = player:GetData().gomorrah
			gomorrah:UseActiveItem(item, flags, slot)

			return true
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, item, rng, player)
	if mod.IsPlayerSodomGomorrah(player) then
		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player), true)
	end
end, CollectibleType.COLLECTIBLE_PINKING_SHEARS)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, item, rng, player)
	if mod.IsPlayerSodomGomorrah(player) then
		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end
end, CollectibleType.COLLECTIBLE_PINKING_SHEARS)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, _, _, player, flags)
	if flags & UseFlag.USE_OWNED == 0 then return end
	local data = player:GetData()

	if data.gomorrah and data.gomorrah:GetData().stopactive then
		data.gomorrah:UseActiveItem(mod.ITEM.STOP, UseFlag.USE_OWNED)
		return
	else
		data.stopactive = not data.stopactive
		if data.stopactive then
			sfx:Play(mod.SOUND.STOP_ENABLE)
		else
			sfx:Play(mod.SOUND.STOP_DISABLE)
		end

		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end

	return true
end, mod.ITEM.STOP)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function()
	if mod.GameHasPlayerSodomGomorrah() then
		mod.GlowingHourglass = true
	end
end, CollectibleType.COLLECTIBLE_GLOWING_HOURGLASS)

mod:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
	if npc.Variant == 0 then
		if npc.Target then
			if npc.Target.Type == EntityType.ENTITY_FAMILIAR and npc.Target.Variant == mod.FAMILIAR.CHAINED_HEART and npc.Target.SubType == 179 then
				npc:Remove()
			end
		end
	end
end, 966)