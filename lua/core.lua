local mod = XalumMods.SodomAndGomorrah

local function Any(value, options)
	for _, test in pairs(options) do
		if test == value then
			return true
		end
	end

	return false
end


function mod.AssignPlayerHUDs()
	local gomorrahs = {}

	for i = 0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(i)
		if mod.IsPlayerGomorrah(player) then
			table.insert(gomorrahs, player)
			player.Parent = player:GetData().sodom
		end
	end

	game:GetHUD():AssignPlayerHUDs()

	for _, player in pairs(gomorrahs) do
		player.Parent = nil
	end
end

function mod.IsPlayerSodom(player, regularOnly, taintedOnly)
	local playerType = player:GetPlayerType()

	if regularOnly then
		return Any(playerType, {
			mod.PLAYER_TYPE.SODOM,
			mod.PLAYER_TYPE.SODOM2
		})
	elseif taintedOnly then
		return playerType == mod.PLAYER_TYPE.SODOM_B
	else
		return Any(playerType, {
			mod.PLAYER_TYPE.SODOM,
			mod.PLAYER_TYPE.SODOM2,
			mod.PLAYER_TYPE.SODOM_B
		})
	end

	return false
end

function mod.IsPlayerGomorrah(player, regularOnly, taintedOnly)
	local playerType = player:GetPlayerType()

	if regularOnly then
		return playerType == mod.PLAYER_TYPE.GOMORRAH
	elseif taintedOnly then
		return playerType == mod.PLAYER_TYPE.GOMORRAH_B
	else
		return Any(playerType, {
			mod.PLAYER_TYPE.GOMORRAH,
			mod.PLAYER_TYPE.GOMORRAH_B
		})
	end

	return false
end

function mod.IsPlayerSodomGomorrah(player, regularOnly, taintedOnly)
	local playerType = player:GetPlayerType()

	if regularOnly then
		return Any(playerType, {
			mod.PLAYER_TYPE.SODOM,
			mod.PLAYER_TYPE.SODOM2,
			mod.PLAYER_TYPE.GOMORRAH
		})
	elseif taintedOnly then
		return Any(playerType, {
			mod.PLAYER_TYPE.SODOM_B,
			mod.PLAYER_TYPE.GOMORRAH_B
		})
	else
		return Any(playerType, {
			mod.PLAYER_TYPE.SODOM,
			mod.PLAYER_TYPE.SODOM2,
			mod.PLAYER_TYPE.GOMORRAH,
			mod.PLAYER_TYPE.SODOM_B,
			mod.PLAYER_TYPE.GOMORRAH_B,
		})
	end

	return false
end

function mod.GameHasPlayerSodomGomorrah()
	for i = 0, game:GetNumPlayers() - 1 do
		if mod.IsPlayerSodomGomorrah(Isaac.GetPlayer(i)) then
			return true
		end
	end

	return false
end

function mod.GetGfxKey(player)
	local playerType = player:GetPlayerType()

	if playerType == mod.PLAYER_TYPE.SODOM or playerType == mod.PLAYER_TYPE.SODOM2 then
		return "sodom"
	elseif playerType == mod.PLAYER_TYPE.GOMORRAH then
		return "gomorrah"
	elseif playerType == mod.PLAYER_TYPE.SODOM_B then
		return "sodom_b"
	elseif playerType == mod.PLAYER_TYPE.GOMORRAH_B then
		return "gomorrah_b"
	end
end