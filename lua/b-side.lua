local mod = XalumMods.SodomAndGomorrah

-- TODO:
--[[
	Mantle & Shield costumes?
	Items aren't appearing in mirror boss as bela
]]

mod.LOCKED_SODOM = Isaac.GetEntityVariantByName("Locked Sodom")

mod.SodomAndGomorrahItemBlacklistB = {
	CollectibleType.COLLECTIBLE_MARKED,
	CollectibleType.COLLECTIBLE_MARS,
	CollectibleType.COLLECTIBLE_ISAACS_HEART,

	CollectibleType.COLLECTIBLE_BOBS_ROTTEN_HEAD, -- Thowables + Throwable-Adjacents
	CollectibleType.COLLECTIBLE_FRIEND_BALL,
	CollectibleType.COLLECTIBLE_DEAD_SEA_SCROLLS,
	CollectibleType.COLLECTIBLE_CANDLE,
	CollectibleType.COLLECTIBLE_RED_CANDLE,
	CollectibleType.COLLECTIBLE_ERASER,
	CollectibleType.COLLECTIBLE_DECAP_ATTACK,
}

function mod.SummonGomorrahB(player)
	local data = player:GetData()

	local gomorrah
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		if p:GetPlayerType() == mod.PLAYER_TYPE.GOMORRAH_B and p.ControllerIndex == player.ControllerIndex then
			gomorrah = p
			break
		end
	end

	if not gomorrah then
		Isaac.ExecuteCommand("addplayer " .. mod.PLAYER_TYPE.GOMORRAH_B .. " " .. player.ControllerIndex)
		gomorrah = Isaac.GetPlayer(game:GetNumPlayers() - 1)
	end

	data.gomorrah = gomorrah
	data.gomorrah.Parent = player

	mod.ChangePlayerAnm2(data.gomorrah, "gomorrah_b")
	mod.ValidateMarked(data.gomorrah)

	data.gomorrah:GetData().sodom = player
	data.gomorrah.ControlsEnabled = false

	local position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, data.gomorrah.Position, Vector.Zero, data.gomorrah)
	position_indicator.Visible = false
	position_indicator:GetSprite():Play("GomorrahB")

	data.gomorrah:GetData().position_indicator = position_indicator

	player.Position = player.Position + Vector(-40, 0)
	data.gomorrah.Position = player.Position + Vector(80, 0)

	game:GetHUD():AssignPlayerHUDs()

	data.gomorrah.Parent = nil

	local sodom_active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)
	local gomorrah_active = data.gomorrah:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

	if not sodom_active and not gomorrah_active then
		player:GetEffects():AddCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE, false, 1)
	end
end

function mod.FilterMarkedTargets(owner)
	local targets = Isaac.FindByType(1000, 30)

	for _, target in pairs(targets) do
		if target.SpawnerEntity:ToPlayer().InitSeed == owner.InitSeed and not target:GetData().sg_controlled then
			target:GetData().sg_controlled = true
			target.SpawnerEntity:GetData().sg_target = target

			return target
		end
	end
end

function mod.FilterVulnerableEnemyNPCs()
	local viables = {}
	for _, entity in pairs(mod.RoomEntities) do
		if entity:IsVulnerableEnemy() and not (entity:HasEntityFlags(EntityFlag.FLAG_FRIENDLY) or entity:HasEntityFlags(EntityFlag.FLAG_NO_TARGET)) then
			table.insert(viables, entity)
		end
	end

	return viables
end

function mod.GetClosestVulnerableEnemyNPC(player)
	local closest
	local dist = 99999

	--print(#mod.VulnerableEnemyNPCs)

	for _, entity in pairs(mod.VulnerableEnemyNPCs) do
		if entity.Position:Distance(player.Position) < dist then
			closest = entity
			dist = entity.Position:Distance(player.Position)
		end
	end

	return closest
end

function mod.ShouldSodomGomorrahFire(player)
	local data = player:GetData()
	local active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)
	local shouldFire = active or mod.HasNonBelaBirthright(player)
	local targetAvailable = mod.VulnerableEnemyNPCs and #mod.VulnerableEnemyNPCs > 0

	return (
		shouldFire and
		targetAvailable and
		not mod.IsJumping(player)
	)
end

function mod.SummonTarget(player)
	player:GetData().forcefire = true
end

function mod.RemoveTarget(player)

end

function mod.HasTarget(player)
	local data = player:GetData()
	return data.sg_target and data.sg_target:Exists()
end

function mod.CalculateGomorrahBInput(player)
	local vec = Vector(0, 0)
	if Input.IsActionPressed(ButtonAction.ACTION_SHOOTUP,	 player.ControllerIndex) then vec = vec + Vector(0, -1) end
	if Input.IsActionPressed(ButtonAction.ACTION_SHOOTDOWN,	 player.ControllerIndex) then vec = vec + Vector(0, 1) end
	if Input.IsActionPressed(ButtonAction.ACTION_SHOOTLEFT,	 player.ControllerIndex) then vec = vec + Vector(-1, 0) end
	if Input.IsActionPressed(ButtonAction.ACTION_SHOOTRIGHT, player.ControllerIndex) then vec = vec + Vector(1, 0) end

	return vec:Normalized()
end

function mod.GetLastGomorrahBInput(player)
	local data = player:GetData()

	local up =	  Input.IsActionPressed(ButtonAction.ACTION_SHOOTUP,	player.ControllerIndex)
	local down =  Input.IsActionPressed(ButtonAction.ACTION_SHOOTDOWN,  player.ControllerIndex)
	local left =  Input.IsActionPressed(ButtonAction.ACTION_SHOOTLEFT,  player.ControllerIndex)
	local right = Input.IsActionPressed(ButtonAction.ACTION_SHOOTRIGHT, player.ControllerIndex)

	if up and not data.lastup then
		data.lastinput = "Up"
	elseif down and not data.lastdown then
		data.lastinput = "Down"
	elseif left and not data.lastleft then
		data.lastinput = "Left"
	elseif right and not data.lastright then
		data.lastinput = "Right"
	end

	data.lastup = up
	data.lastdown = down
	data.lastleft = left
	data.lastright = right
end

function mod.PlayGomorrahBDirection(player)
	local data = player:GetData()
	local sprite = player:GetSprite()

	if player:IsExtraAnimationFinished() then
		sprite:Play("Walk" .. data.lastinput)
		sprite:PlayOverlay("Head" .. data.lastinput)
	end
end

function mod.CheckActivation(player)
	local data = player:GetData()
	local active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

	if data.wasactive == nil then data.wasactive = active end

	if active and not data.wasactive then
		mod.OnActivation(player)
	elseif data.wasactive and not active then
		mod.OnDeactivation(player)
	end

	data.wasactive = active
end

function mod.OnActivation(player)
	local data = player:GetData()

	data.forcefire = true
	sfx:Play(SoundEffect.SOUND_BALL_AND_CHAIN_HIT, 1, 0, false, 0.8)
end

function mod.OnDeactivation(player)
	local data = player:GetData()

	if data.sg_target and not mod.HasNonBelaBirthright(player) then
		data.ScheduleTargetRemove = player.FrameCount
	end
end

function mod.LimitHealthType(player)
	local containers = player:GetMaxHearts()
	local hearts = player:GetHearts()

	player:AddHearts(-math.min(containers, hearts))
	player:AddMaxHearts(-containers)

	player:AddSoulHearts(containers)
end

--[[function mod.EqualiseItems(ActivePlayer, InertPlayer)
	local item = ActivePlayer:GetActiveItem(0)
	local charge = ActivePlayer:GetActiveCharge(0)

	if InertPlayer:GetActiveItem() > 0 and item <= 0 then
		print("Inert Charge", InertPlayer:GetActiveCharge(), "\nInert Item", InertPlayer:GetActiveItem(), "\nActive Item", item, "\n")

		--ActivePlayer:AddCollectible(item, InertPlayer:GetActiveCharge(), false)

		local config = Isaac.GetItemConfig()
		ActivePlayer:QueueItem(config:GetCollectible(item), InertPlayer:GetActiveCharge())
	elseif InertPlayer:GetActiveCharge() ~= charge then
		print("Active Charge", charge)

		InertPlayer:SetActiveCharge(charge)
	end

	local trinket = ActivePlayer:GetTrinket(0)

	local card = ActivePlayer:GetCard(0)
	local pill = ActivePlayer:GetPill(0)
end]]

function mod.LostCurseParityB(me, you)
	if not (me and you) then return end

	if mod.GetLostCurseStatus(me) and not mod.GetLostCurseStatus(you) then
		local active = me:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

		mod.MakeWhiteFireGhost(you)

		if mod.IsPlayerSodom(me) then
			me:UseActiveItem(mod.ITEM.TAG_SODOM, UseFlag.USE_NOANIM)
			you:UseActiveItem(mod.ITEM.TAG_GOMORRAH, UseFlag.USE_NOANIM)
		else
			you:UseActiveItem(mod.ITEM.TAG_SODOM, UseFlag.USE_NOANIM)
			me:UseActiveItem(mod.ITEM.TAG_GOMORRAH, UseFlag.USE_NOANIM)
		end

		for _, player in pairs({me, you}) do
			player:ChangePlayerType(mod.PLAYER_TYPE.BELA)
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_MARKED)
			player:ClearCostumes()
			player.SpriteOffset = Vector.Zero
		end

		me.Visible = active
		--me.ControlsEnabled = active

		you.Visible = not active
		--you.ControlsEnabled = not active
	end
end

function mod.ValidateMarked(player)
	if not player:HasCollectible(CollectibleType.COLLECTIBLE_MARKED) and not player:HasCurseMistEffect() then
		player:AddCollectible(CollectibleType.COLLECTIBLE_MARKED, 0, false)
	end
end

function mod.HasNonBelaBirthright(player)
	return player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT)
end

function mod.FixBirthrightPickupB(player, bela)
	if player.FrameCount > 0 then
		local has = player:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT)
		local data = player:GetData()

		if data.sodomgomorrah_hasbirthright == nil then data.sodomgomorrah_hasbirthright = has end

		if data.sodomgomorrah_hasbirthright ~= has then
			if bela then
				player:GetEffects():AddCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT, false, 1)

				player:AddMaxHearts(4)
				player:AddHearts(4)

				player:AddCacheFlags(CacheFlag.CACHE_FAMILIARS + CacheFlag.CACHE_FIREDELAY)
				player:EvaluateItems()

				print("Adding Bela's birthright")
			else
				player:AddCacheFlags(CacheFlag.CACHE_FAMILIARS + CacheFlag.CACHE_FIREDELAY)
				player:EvaluateItems()

				print("Adding birthright")
			end
		end

		data.sodomgomorrah_hasbirthright = has
	end
end

function mod.SodomGommorahCommonB(player, movement)
	local data = player:GetData()
	local other = data.sodom or data.gomorrah
	player:ClearCostumes()

	mod.ValidateMarked(player)
	mod.CheckActivation(player)

	if data.UnBelaReskin then
		data.UnBelaReskin = nil

		player.Visible = true
		player.ControlsEnabled = true
		player.EntityCollisionClass = 4

		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end

	local any_bela_birthright = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT) or other:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT)
	if not any_bela_birthright then
		mod.LimitHealthType(player)
	end

	if player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and player.FrameCount % 6 == 0 and data.lastcheckframe ~= player.FrameCount and not player:IsDead() and player:IsExtraAnimationFinished() then
		data.lastcheckframe = player.FrameCount
		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player), false, player.FrameCount % 12 == 0)
	end

	if data.diewhenready and not other:IsHoldingItem() then
		data.passdamage = true
		player:TakeDamage(1, 0, EntityRef(player), 0)
		data.diewhenready = false
	end

	if movement:Length() > 0 and mod.CanPlayerJump(player, true) then
		mod.CheckMover(player, movement, true)
	end

	if not data.position_indicator then
		data.position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, player.Position, Vector.Zero, player)
		data.position_indicator.Visible = false

		if mod.IsPlayerSodom(player) then
			data.position_indicator:GetSprite():Play("SodomB")
		else
			data.position_indicator:GetSprite():Play("GomorrahB")
		end
	end

	if data.jump_velocity then
		if mod.savedata.position_indicator == 2 then
			data.position_indicator.Position = player.Position
		elseif mod.savedata.position_indicator == 3 then
			local frames = 0
			local jump_vel = data.jump_velocity
			local off = player.SpriteOffset.Y

			repeat
				off = off - player.MoveSpeed * jump_vel / 2
				jump_vel = jump_vel - player.MoveSpeed / 2

				frames = frames + 1
			until off - jump_vel > 0

			data.position_indicator.Position = mod.Lerp(data.position_indicator.Position, player.Position + player.Velocity * frames, 0.25)
		end

		mod.DoJump(player, movement, true)
	else
		player.Velocity = player.Velocity * 0.6
		data.position_indicator.Position = player.Position
	end

	if data.tosummontarget then
		local found = mod.FilterMarkedTargets(player)
		if found then
			data.tosummontarget = false
			data.forcefire = false
		end
	end

	if mod.HasTarget(player) then
		local target = mod.GetClosestVulnerableEnemyNPC(player)
		if target then
			data.ScheduleTargetRemove = nil
			data.sg_target.Velocity = target.Position + target.Velocity - data.sg_target.Position
		else
			data.ScheduleTargetRemove = data.ScheduleTargetRemove or player.FrameCount
		end
	elseif mod.ShouldSodomGomorrahFire(player) then
		mod.SummonTarget(player)
	end

	if data.ScheduleTargetRemove and data.ScheduleTargetRemove + 1 < player.FrameCount then
		data.sg_target:Remove()
		data.sg_target = nil

		data.ScheduleTargetRemove = nil
	end

	data.landinglag = data.landinglag or 0
	if data.landinglag > 0 then
		data.landinglag = data.landinglag - 1
		player.Velocity = Vector.Zero
	end

	if player:GetSprite():IsPlaying("TeleportDown") then
		player.Velocity = Vector.Zero

		if player:GetSprite():GetFrame() == 11 and not other:GetSprite():IsPlaying("TeleportDown") then
			other:PlayExtraAnimation("TeleportDown")
			other.Position = player.Position

			for i = 1, 10 do other:GetSprite():Update() end
		end
	end

	if data.RedBombCollision then
		if player:IsHoldingItem() then
			if movement:Length() > 0 then
				player:ThrowHeldEntity(movement:Resized(15))
			end
		else
			data.RedBombCollision = false
		end
	end

	if player:HasCollectible(CollectibleType.COLLECTIBLE_TOOTH_AND_NAIL) and mod.DidToothAndNailJustChange(player) then
		mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	end

	mod.LostCurseParityB(player, other)
	mod.SpiritShackleParity(player, other)
	mod.FixBirthrightPickupB(player)

	data.lastvel = player.Velocity
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	local player = Isaac.GetPlayer()

	if mod.IsPlayerSodomGomorrah(player, false, true) and not mod.savedata.unlocked_tainted then
		local level = game:GetLevel()
		if level:GetStage() == LevelStage.STAGE8 then
			level:ChangeRoom(95)

			player.Position = Vector(245, 280)

			player:SetPocketActiveItem(CollectibleType.COLLECTIBLE_RED_KEY, ActiveSlot.SLOT_POCKET2)
			player:UseActiveItem(CollectibleType.COLLECTIBLE_RED_KEY, UseFlag.USE_OWNED + UseFlag.USE_NOANIM, ActiveSlot.SLOT_POCKET2)
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_RED_KEY)

			sfx:Stop(SoundEffect.SOUND_UNLOCK00)

			player.Position = Vector(160, 280)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	local player = Isaac.GetPlayer()

	if mod.IsPlayerSodomGomorrah(player, false, true) and not mod.savedata.unlocked_tainted then
		local level = game:GetLevel()
		local desc = level:GetCurrentRoomDesc()

		if level:GetStage() == LevelStage.STAGE8 and desc.SafeGridIndex == 94 then
			local room = game:GetRoom()
			local centre = room:GetCenterPos() + Vector(0, 0)

			for _, shopkeeper in pairs(Isaac.FindByType(17)) do
				shopkeeper:Remove()
			end

			for _, item in pairs(Isaac.FindByType(5)) do
				item:Remove()
			end

			Isaac.Spawn(6, mod.LOCKED_SODOM, 0, centre, Vector.Zero, nil)

			local door = room:GetDoor(2)
			room:RemoveGridEntity(door:GetGridIndex(), 0)

			for i = 1, 3 do
				Isaac.Spawn(1000, 21, 0, centre, Vector.Zero, nil)
			end

			for i = 1, 8 do
				local gib = Isaac.Spawn(1000, 163, 0, centre + Vector(0, 10) + RandomVector():Resized(40), Vector.Zero, nil)
				gib:AddEntityFlags(EntityFlag.FLAG_RENDER_FLOOR)
			end

			Isaac.Spawn(1000, 64, 0, centre, Vector.Zero, nil)
		end
	end

	for _, flame in pairs(Isaac.FindByType(1000, mod.EFFECT.BELA_FIRE)) do
		flame.Visible = true

		if flame.SpawnerEntity then
			flame.Position = flame.SpawnerEntity.Position
		end
	end

	if mod.CURRENT_DIMENSION == 1 and game:GetLevel():GetStage() == LevelStage.STAGE2_2 then
		for _, p in pairs(Isaac.FindByType(1)) do
			player = p:ToPlayer()

			if mod.IsPlayerSodom(player, false, true) then
				local gomorrah = player:GetData().gomorrah
				local active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

				for _, curr in pairs({player, gomorrah}) do
					curr:ChangePlayerType(mod.PLAYER_TYPE.BELA)
					curr:ClearCostumes()

					curr:RemoveCollectible(CollectibleType.COLLECTIBLE_MARKED)
					curr:GetEffects():AddCollectibleEffect(CollectibleType.COLLECTIBLE_INNER_EYE, 1)

					curr:AddCacheFlags(CacheFlag.CACHE_ALL)
					curr:EvaluateItems()

					curr.SpriteOffset = Vector.Zero
				end

				player.Visible = active
				player.ControlsEnabled = active

				gomorrah.Visible = not active
				gomorrah.ControlsEnabled = not active
			end
		end
	end

	if game:GetLevel():GetStage() == LevelStage.STAGE3_2 and #Isaac.FindByType(78) > 0 then
		for _, p in pairs(Isaac.FindByType(1)) do
			local player = p:ToPlayer()

			if mod.IsPlayerSodomGomorrah(player, false, true) then
				if player:HasCollectible(CollectibleType.COLLECTIBLE_KNIFE_PIECE_1) then
					player:RemoveCollectible(CollectibleType.COLLECTIBLE_KNIFE_PIECE_1)
				end

				if player:HasCollectible(CollectibleType.COLLECTIBLE_KNIFE_PIECE_2) then
					player:RemoveCollectible(CollectibleType.COLLECTIBLE_KNIFE_PIECE_2)
				end
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_UPDATE, function(_, player)
	local data = player:GetData()

	if player:GetPlayerType() == mod.PLAYER_TYPE.SODOM_B then
		if not mod.savedata.unlocked_tainted then
			player.ControlsEnabled = false
			player.Visible = false

			local hud = game:GetHUD()
			hud:SetVisible(false)

			local level = game:GetLevel()
			if game.Difficulty < Difficulty.DIFFICULTY_GREED then
				if level:GetStage() ~= LevelStage.STAGE8 then
					Isaac.ExecuteCommand("stage 13")
				end
			end
		else
			mod.RoomEntities = Isaac.GetRoomEntities()
			mod.VulnerableEnemyNPCs = mod.FilterVulnerableEnemyNPCs()

			local movement = player:GetMovementInput():Normalized()

			if data.gomorrah then
				if data.nohpcheck then
					data.nohpcheck = nil
					mod.MakeHpTemplate(player)
				else
					mod.CheckHpTemplate(player, data.gomorrah)
				end

				if data.gomorrah_pillcard and Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex) then
					local card = player:GetCard(0)
					local pill = player:GetPill(0)

					player:DropPocketItem(0, player.Position)

					if card > 0 then
						data.gomorrah:UseCard(card)

						local pickup = mod.GetClosest(player.Position, 5, 300)
						if pickup then pickup:Remove() end
					elseif pill > 0 then
						local effect = game:GetItemPool():GetPillEffect(pill, data.gomorrah)
						data.gomorrah:UsePill(effect, pill)

						local pickup = mod.GetClosest(player.Position, 5, 70)
						if pickup then pickup:Remove() end
					end

					data.gomorrah_pillcard = false
				end

				if mod.IsChallengeBoundBySteel() then
					if not data.jump_velocity then
						mod.LimitStationaryChainLength(player, data.gomorrah)
					end

					if not data.challenge_chains or not data.challenge_chains[1]:Exists() then
						data.challenge_chains = {}

						for i = 1, 9 do
							data.challenge_chains[i] = Isaac.Spawn(1000, mod.EFFECT.CHAINED_HEART_CHAIN, 0, player.Position, Vector.Zero, player)
						end
					end

					for index, link in pairs(data.challenge_chains) do
						link.SpriteScale = player.SpriteScale + (data.gomorrah.SpriteScale - player.SpriteScale) * index/9

						if player.FrameCount == 0 then
							link.Position = (player.Position - data.gomorrah.Position) * index / 9 + data.gomorrah.Position + Vector(0, -0.1)
						else
							link.Velocity = (player.Position - data.gomorrah.Position) * index / 9 + data.gomorrah.Position - link.Position
						end
					end

					if player.SpriteOffset.Y >= data.gomorrah.SpriteOffset.Y then
						for index, link in pairs(data.challenge_chains) do
							link.SpriteOffset = player.SpriteOffset + Vector(0, -math.abs(player.SpriteOffset.Y - data.gomorrah.SpriteOffset.Y)) * (9 - index) / 9
						end
					else
						for index, link in pairs(data.challenge_chains) do
							link.SpriteOffset = data.gomorrah.SpriteOffset + Vector(0, -math.abs(data.gomorrah.SpriteOffset.Y - player.SpriteOffset.Y)) * index / 9
						end
					end
				end

				mod.RatifyRevivers(player, data.gomorrah)
				mod.FixHeartbreakPickup(player)

				local active = player:GetActiveItem()
				data.hashadactive = data.hashadactive or {}
				data.hashadactive[tostring(active)] = true

				mod.SodomGommorahCommonB(player, movement)
			else
				mod.SummonGomorrahB(player)

				if not player:HasCollectible(mod.ITEM.SWITCH) then
					player:SetPocketActiveItem(mod.ITEM.SWITCH, ActiveSlot.SLOT_POCKET, false)
				end
				mod.ValidateMarked(player)

				player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				player:EvaluateItems()

				data.position_indicator = Isaac.Spawn(1000, mod.EFFECT.POSITION_TARGET, 0, player.Position, Vector.Zero, player)
				data.position_indicator.Visible = false
				data.position_indicator:GetSprite():Play("SodomB")

				mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
			end
		end
	elseif player:GetPlayerType() == mod.PLAYER_TYPE.GOMORRAH_B then
		local movement = mod.CalculateGomorrahBInput(player)

		if not mod.savedata.unlocked_tainted then
			player.ControlsEnabled = false
			player.Visible = false
		end

		if data.sodom then
			player.ControlsEnabled = data.sodom.ControlsEnabled

			local ticks = (data.sodom:GetTrinket(0) == TrinketType.TRINKET_TICK and 1 or 0) + (data.sodom:GetTrinket(1) == TrinketType.TRINKET_TICK and 1 or 0)
			data.canpicktrinkets = ticks ~= mod.CalculateTrinketPocketSize(data.sodom)

			if not player:IsDead() then
				mod.TransferMainOnlyItems(player, data.sodom)
				mod.TransferMainOnlyItemTypes(player, data.sodom)

				mod.FixBatteryPackPickup(player)
			end

			if Input.IsActionTriggered(ButtonAction.ACTION_BOMB, player.ControllerIndex) and not player:HasGoldenBomb() then
				local valid = 0
				for _, bomb in pairs(Isaac.FindByType(4)) do
					if bomb.FrameCount < 1 then
						valid = valid + 1
					end
				end

				if valid > 1 then
					player:AddBombs(1)
				elseif valid == 1 then
					Isaac.Spawn(4, 0, 0, player.Position, Vector.Zero, player):ToBomb().Flags = player:GetBombFlags()
				end
			end

			mod.GetLastGomorrahBInput(player)
			if data.jump_velocity then
				mod.PlayGomorrahBDirection(player)
			end

			mod.SodomGommorahCommonB(player, movement)
		end
	elseif player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
		local sprite = player:GetSprite()
		local other = data.sodom or data.gomorrah
		local effects = player:GetEffects()
		local other_effects

		if other then
			other_effects = other:GetEffects()
		end

		local any_bela_birthright = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT) or (other and other:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_BELAS_BIRTHRIGHT))
		if not any_bela_birthright then
			mod.LimitHealthType(player)
		end

		if effects:HasCollectibleEffect(mod.ITEM.TAG_SODOM) or effects:HasCollectibleEffect(mod.ITEM.TAG_GOMORRAH) then
			if not other then
				for _, p in pairs(Isaac.FindByType(1)) do
					p = p:ToPlayer()

					if p:GetPlayerType() == mod.PLAYER_TYPE.BELA and p:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_GOMORRAH) and p.ControllerIndex == player.ControllerIndex then
						data.gomorrah = p
						p:GetData().sodom = player
						p.Parent = nil

						other = p
					end
				end
			end

			if other then
				if player:IsDead() then
					other:Die()
				else
					Isaac.DebugString("Start")

					if data.gomorrah then
						mod.CheckHpTemplate(player, data.gomorrah)
					end

					if not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and not mod.TimeTravelling then
						player.Visible = false
						--player.ControlsEnabled = false
						player.EntityCollisionClass = 0
						player.Velocity = Vector.Zero

						player.Position = other.Position
					end

					if mod.CURRENT_DIMENSION == 0 and not mod.GetLostCurseStatus(player) then
						local character = data.sodom and mod.PLAYER_TYPE.GOMORRAH_B or mod.PLAYER_TYPE.SODOM_B
						local character2 = data.sodom and mod.PLAYER_TYPE.SODOM_B or mod.PLAYER_TYPE.GOMORRAH_B

						local other = data.sodom or data.gomorrah

						player:ChangePlayerType(character)
						other:ChangePlayerType(character2)

						for _, char in pairs({player, other}) do
							local dat = char:GetData()
							if dat.flames then
								dat.flames:Remove()
								dat.flames = nil
							end

							dat.UnBelaReskin = true
							char:AddCollectible(CollectibleType.COLLECTIBLE_MARKED, 0, false)
						end

						local sodom = data.sodom or player
						sodom:SetPocketActiveItem(mod.ITEM.SWITCH, ActiveSlot.SLOT_POCKET, false)
					end
				end
			end
		end

		if player.Visible then
			if not data.flames then
				data.flames = Isaac.Spawn(1000, mod.EFFECT.BELA_FIRE, 0, player.Position, Vector.Zero, player):ToEffect()
				data.flames.EntityCollisionClass = 0
				data.flames.SpriteOffset = Vector(0, -4)

				data.flames:GetSprite():Play("Idle")
				data.flames:AddEntityFlags(EntityFlag.FLAG_PERSISTENT)
			end
		end

		if data.flames then
			local anim = sprite:GetAnimation()
			local show = player.Visible and (
				sprite:GetOverlayAnimation() ~= "" or
				anim == "PickupWalkDown" or
				anim == "PickupWalkUp" or
				anim == "PickupWalkLeft" or
				anim == "PickupWalkRight"
			)

			data.flames.Visible = show

			data.flames.Position = player.Position + Vector(0, -1)
		end

		local anm2 = sprite:GetFilename()
		if anm2 ~= "gfx/player_bela.anm2" and anm2 ~= "gfx/player_bela_nodeath.anm2" then
			if player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) or not other then
				sprite:Load("gfx/player_bela.anm2", true)
			else
				sprite:Load("gfx/player_bela_nodeath.anm2", true)
			end
			sprite:Play("WalkDown")
		end

		local effects = player:GetEffects()
		if not effects:HasCollectibleEffect(CollectibleType.COLLECTIBLE_INNER_EYE) then
			if mod.GetLostCurseStatus(player) or not other or player:HasCurseMistEffect() then
				effects:AddCollectibleEffect(CollectibleType.COLLECTIBLE_INNER_EYE, 1)
			end
		end

		if mod.GetLostCurseStatus(player) or not other then
			mod.FixBirthrightPickupB(player, true)
		end

		player:ClearCostumes()
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_RENDER, function(_, effect)
	if effect.SpawnerEntity then
		effect.Position = effect.SpawnerEntity.Position + Vector(0, -1)
		--effect.Visible = effect.SpawnerEntity.Visible
	end
end, mod.EFFECT.BELA_FIRE)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	if mod.IsPlayerSodomGomorrah(player, false, true) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
		if mod.HasNonBelaBirthright(player) then
			player.MaxFireDelay = player.MaxFireDelay * 2
		else
			player.MaxFireDelay = 999999
		end
	end
end, CacheFlag.CACHE_FIREDELAY)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	if player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
		player.CanFly = true
	end
end, CacheFlag.CACHE_FLYING)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	local playertype = player:GetPlayerType()
	local data = player:GetData()

	if mod.HasNonBelaBirthright(player) then
		if playertype == mod.PLAYER_TYPE.SODOM_B then
			player:CheckFamiliar(mod.FAMILIAR.ADMAH_B, player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and 1 or 0, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
		elseif playertype == mod.PLAYER_TYPE.GOMORRAH_B then
			player:CheckFamiliar(mod.FAMILIAR.ZEBOIM_B, player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and 1 or 0, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
		elseif playertype == mod.PLAYER_TYPE.BELA then
			if data.sodom then -- I AM GOMORRAH
				player:CheckFamiliar(mod.FAMILIAR.ZEBOIM_B, 1, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
			elseif data.gomorrah then -- I AM SODOM
				player:CheckFamiliar(mod.FAMILIAR.ADMAH_B, 1, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
			end
		end
	elseif playertype == mod.PLAYER_TYPE.BELA then
		if data.sodom then -- I AM GOMORRAH
			player:CheckFamiliar(mod.FAMILIAR.ZEBOIM_B, 0, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
		elseif data.gomorrah then -- I AM SODOM
			player:CheckFamiliar(mod.FAMILIAR.ADMAH_B, 0, player:GetCollectibleRNG(CollectibleType.COLLECTIBLE_BIRTHRIGHT))
		end
	end
end, CacheFlag.CACHE_FAMILIARS)

mod:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function()
	for _, player in pairs(Isaac.FindByType(1)) do
		player = player:ToPlayer()

		if player:GetPlayerType() == mod.PLAYER_TYPE.SODOM_B then
			if #Isaac.FindByType(1000, mod.EFFECT.BELA) == 0 then
				player:UseActiveItem(mod.ITEM.SWITCH, UseFlag.USE_NOANIM + UseFlag.USE_OWNED)
				sfx:Play(SoundEffect.SOUND_BALL_AND_CHAIN_HIT)
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, item, rng, player, flags, slot, custom)
	if player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
		local data = player:GetData()
		local other = data.sodom or data.gomorrah

		if other and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
			other:UseActiveItem(item, flags, slot)
			return true
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, item, rng, player, flags)
	if flags & UseFlag.USE_OWNED == 0 then return end
	if #Isaac.FindByType(1000, mod.EFFECT.BELA) ~= 0 then
		return {
			Discharge = false,
			ShowAnim = false,
		}
	end

	local data = player:GetData()
	local other = data.gomorrah or data.sodom
	local data2 = other:GetData()

	local effects = player:GetEffects()
	local effects2 = other:GetEffects()

	local any_active = effects:HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) or effects2:HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

	if not any_active then
		return {Discahrge = false, ShowAnim = false}
	end

	local active = effects:HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and player or other
	local non_active = effects2:HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) and player or other

	effects:RemoveCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE, effects:GetCollectibleEffectNum(mod.ITEM.TAG_SWITCH_ACTIVE))
	effects2:RemoveCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE, effects2:GetCollectibleEffectNum(mod.ITEM.TAG_SWITCH_ACTIVE))

	sfx:Play(SoundEffect.SOUND_BALL_AND_CHAIN_HIT)

	player:AddCacheFlags(CacheFlag.CACHE_FIREDELAY + CacheFlag.CACHE_FAMILIARS)
	player:EvaluateItems()

	other:AddCacheFlags(CacheFlag.CACHE_FIREDELAY + CacheFlag.CACHE_FAMILIARS)
	other:EvaluateItems()

	mod.ChangePlayerAnm2(player, mod.GetGfxKey(player))
	mod.ChangePlayerAnm2(other, mod.GetGfxKey(other))

	local fire_angle = (non_active.Position - active.Position):GetAngleDegrees()
	if rng:RandomFloat() < 0.5 then
		fire_angle = fire_angle + 135
	else
		fire_angle = fire_angle - 135
	end

	local bela = Isaac.Spawn(1000, mod.EFFECT.BELA, 0, active.Position, Vector.FromAngle(fire_angle):Resized(10), player)
	bela:GetData().sodomGomorarhTarget = non_active
	bela.SpriteOffset = active.SpriteOffset

	if flags & UseFlag.USE_NOANIM == 0 then
		return true
	end
end, mod.ITEM.SWITCH)

mod:AddPriorityCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, CallbackPriority.IMPORTANT, function(_, entity, amount, flags, source, cooldown)
	local player = entity:ToPlayer()
	local data = player:GetData()

	if mod.IsPlayerSodomGomorrah(player) then
		if source.Type == EntityType.ENTITY_FAMILIAR and source.Variant == FamiliarVariant.BLOOD_OATH then
			return
		end
	end

	if mod.IsPlayerSodomGomorrah(player, false, true) then
		if data.passdamage then return end

		local active = player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE)

		if flags & DamageFlag.DAMAGE_PITFALL > 0 and not active then return false end
		if (data.jump_velocity or not active) and flags & mod.SODOM_GOMORRAH_DAMAGE_FLAG_MASK == 0 and not mod.DamageIsSacrifice(flags) and not mod.ShouldEntityPassDamage(source) then return false end

		local other = data.sodom or data.gomorrah
		local sodom = data.sodom or player
		local gomorrah = data.gomorrah or player

		if amount >= mod.CalcPlayerHealth(player) then
			if mod.HasCard(sodom, Card.CARD_SOUL_LAZARUS) then
				gomorrah:AddCard(Card.CARD_SOUL_LAZARUS)
			end
		end

		if mod.DamageIsSacrifice(flags) or mod.DamageIsMausDoor(flags, source) then
			flags = 0
		end

		other:GetData().passdamage = true
		other:TakeDamage(amount, flags, source, cooldown)
		other:GetData().passdamage = false
	end
end, 1)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action <= ButtonAction.ACTION_SHOOTDOWN then
		local player = entity and entity:ToPlayer()
		if not player then return end
		local data = player:GetData()

		if player:GetPlayerType() == mod.PLAYER_TYPE.BELA and (data.sodom or data.gomorrah) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then return 0 end

		if action <= ButtonAction.ACTION_DOWN then
			if player:GetPlayerType() == mod.PLAYER_TYPE.GOMORRAH_B then return 0 end
		end

		if action == ButtonAction.ACTION_SHOOTDOWN then
			if data.forcefire then
				local target = mod.GetClosestVulnerableEnemyNPC(player)

				if target then
					data.tosummontarget = true
					player.FireDelay = player.MaxFireDelay

					return 1
				else
					data.forcefire = false
				end
			end
		end

		if action >= ButtonAction.ACTION_SHOOTLEFT then
			if mod.IsPlayerSodomGomorrah(player, false, true) then
				if not data.tosummontarget then return 0 end
				if data.jump_velocity then return 0 end
			end
		end
	end
end, InputHook.GET_ACTION_VALUE)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action >= ButtonAction.ACTION_SHOOTLEFT and action <= ButtonAction.ACTION_SHOOTDOWN then
		local player = entity and entity:ToPlayer()
		if not player then return end

		local data = player:GetData()
		if mod.IsPlayerSodomGomorrah(player, false, true) then
			return false
		elseif player:GetPlayerType() == mod.PLAYER_TYPE.BELA and (data.sodom or data.gomorrah) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
			return false
		end
	end
end, InputHook.IS_ACTION_PRESSED)

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if action == ButtonAction.ACTION_BOMB then
		local player = entity and entity:ToPlayer()
		if not player then return end
		local data = player:GetData()

		if player:GetPlayerType() == mod.PLAYER_TYPE.BELA and (data.sodom or data.gomorrah) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
			return false
		end
	end
end, InputHook.IS_ACTION_TRIGGERED)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_INIT, function(_, effect)
	effect:AddEntityFlags(EntityFlag.FLAG_PERSISTENT)
end, mod.EFFECT.BELA)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	local data = effect:GetData()
	local sprite = effect:GetSprite()
	local player = effect.SpawnerEntity and effect.SpawnerEntity:ToPlayer()

	if not data.sodomGomorarhTarget then
		effect:Remove()
		return
	end

	data.state = data.state or 0

	for _, entity in pairs(Isaac.FindInRadius(effect.Position, effect.Size + 10, EntityPartition.ENEMY)) do
		local damage = 1
		if player then damage = player.Damage * 5 end

		entity:TakeDamage(damage, DamageFlag.DAMAGE_FIRE, EntityRef(effect), 0)
		entity:AddBurn(EntityRef(effect), 30, 40)
	end

	if sprite:IsFinished("Charge") then
		data.state = 2
	end

	if data.state == 0 and effect.FrameCount >= 15 then
		data.state = 1
		sprite:Play("Charge")
	end

	if data.state == 2 then
		effect.SpriteOffset = mod.Lerp(effect.SpriteOffset, data.sodomGomorarhTarget.SpriteOffset, 0.1)

		local target_vel = (data.sodomGomorarhTarget.Position - effect.Position):Resized(30)
		effect.Velocity = mod.Lerp(effect.Velocity, target_vel, 0.1)

		local anim = "Dash"

		if math.abs(effect.Velocity.Y) > math.abs(effect.Velocity.X) then
			if effect.Velocity.Y > 0 then
				anim = anim .. "Down"
			else
				anim = anim .. "Up"
			end

			sprite.FlipX = false
		else
			anim = anim .. "Hori"
			sprite.FlipX = effect.Velocity.X < 0
		end

		if sprite:IsFinished("Charge") then
			sprite:Play(anim)
		end

		sprite:SetAnimation(anim, false)
	else
		effect.Velocity = effect.Velocity * 0.9
	end

	if effect.Position:Distance(data.sodomGomorarhTarget.Position) <= effect.Size then
		local effects = data.sodomGomorarhTarget:ToPlayer():GetEffects()
		effects:AddCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE, false, 1)

		data.sodomGomorarhTarget:AddCacheFlags(CacheFlag.CACHE_FIREDELAY + CacheFlag.CACHE_FAMILIARS)
		data.sodomGomorarhTarget:EvaluateItems()

		effect:Remove()
	end
end, mod.EFFECT.BELA)

for _, variant in pairs({mod.FAMILIAR.ADMAH_B, mod.FAMILIAR.ZEBOIM_B}) do
	mod:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, function(_, familiar)
		familiar:AddToOrbit(mod.FAMILIAR.ADMAH_B)
		familiar:GetSprite():Play("Float")
	end, variant)

	mod:AddCallback(ModCallbacks.MC_PRE_FAMILIAR_COLLISION, function(_, familiar, collider)
		if collider:ToProjectile() then
			mod.AddInfamyTint(familiar)
			collider:Die()
		elseif collider:ToNPC() then
			collider:AddBurn(EntityRef(familiar.Player), 90, familiar.Player.Damage)
		end
	end, variant)
end

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	familiar.OrbitDistance = Vector(35, 35)
	familiar.OrbitSpeed = -0.05
	familiar.Velocity = familiar:GetOrbitPosition(familiar.Player.Position + familiar.Player.Velocity) - familiar.Position
end, mod.FAMILIAR.ADMAH_B)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	familiar.OrbitDistance = Vector(35, 35)
	familiar.OrbitSpeed = 0.05
	familiar.Velocity = familiar:GetOrbitPosition(familiar.Player.Position + familiar.Player.Velocity) - familiar.Position
end, mod.FAMILIAR.ZEBOIM_B)

mod:AddCallback(ModCallbacks.MC_POST_FIRE_TEAR, function(_, tear)
	local player = tear.SpawnerEntity and tear.SpawnerEntity:ToPlayer()

	if player and mod.IsPlayerSodomGomorrah(player, false, true) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
		local dust = Isaac.Spawn(1000, 145, 0, tear.Position, tear.Velocity, nil):ToEffect()
		dust.SpriteOffset = Vector(0, tear.Height)

		sfx:Play(SoundEffect.SOUND_SIREN_MINION_SMOKE, 0.6, 0, false, math.random(8, 11)/10)
		sfx:Stop(SoundEffect.SOUND_TEARS_FIRE)
	end
end)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	local player = familiar.Player

	if player:GetPlayerType() == mod.PLAYER_TYPE.BELA then
		local data = player:GetData()

		if (data.sodom or data.gomorrah) and not player:GetEffects():HasCollectibleEffect(mod.ITEM.TAG_SWITCH_ACTIVE) then
			familiar:Remove()
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, familiar)
	if mod.IsPlayerSodomGomorrah(familiar.Player, false, true) then
		local room = game:GetRoom()

		if room:IsClear() and room:GetType() == RoomType.ROOM_BOSS then
			local level = game:GetLevel()

			if level:GetStage() == LevelStage.STAGE3_2 and level:GetStageType() >= StageType.STAGETYPE_REPENTANCE then
				local door

				for i = 0, 3 do
					door = room:GetDoor(i)
					if door then
						local sprite = door:GetSprite()
						if sprite:GetFilename() == "gfx/grid/Door_MomsHeart.anm2" and door:IsLocked() then
							door:TryUnlock(familiar.Player, true)
						end
					end
				end
			end
		end
	end
end, FamiliarVariant.KNIFE_FULL)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, _, _, player)
	if mod.IsPlayerSodomGomorrah(player, false, true) then
		if player:HasCollectible(CollectibleType.COLLECTIBLE_MARKED) then
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_MARKED)
		end
	end
end, CollectibleType.COLLECTIBLE_D4)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, _, _, player)
	if mod.IsPlayerSodomGomorrah(player, false, true) then
		if player:HasCollectible(CollectibleType.COLLECTIBLE_MARKED) then
			player:RemoveCollectible(CollectibleType.COLLECTIBLE_MARKED)
		end
	end
end, CollectibleType.COLLECTIBLE_D100)