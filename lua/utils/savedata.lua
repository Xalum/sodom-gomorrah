local mod = XalumMods.SodomAndGomorrah
local json = require "json"

local function Validate(key, default)
	local newValue = mod.savedata[key] ~= nil and mod.savedata[key] or default
	mod.savedata[key] = newValue
end

function mod.SaveSaveData()
	mod:SaveData(json.encode(mod.savedata))
end

function mod.LoadSaveData()
	if not mod:HasData() or mod:LoadData() == "null" then
		mod.WipePersistentSaveData(false, true)
	end

	mod.savedata = json.decode(mod:LoadData())

	Validate("speed_mode", 2)
	Validate("position_indicator", 1)
	Validate("pickup_magnet", 1)
	Validate("challenge_mode", "none")

	Validate("beat_pillar_of_salt", false)
	Validate("beat_cardiac_arrest", false)
	Validate("beat_bound_by_steel", false)

	Validate("skin_a", "default")
	Validate("skin_b", "default")
	Validate("unlocked_tainted", false)

	Validate("beat_basement", 	false)
	Validate("beat_caves", 		false)
	Validate("beat_depths", 	false)
	Validate("beat_downpour", 	false)
	Validate("beat_mines",		false)
	Validate("beat_mausoleum",	false)

	Validate("cityboy", false)
	Validate("cityboytwo", false)
end

--[[function mod.ResetSaveData() -- Save & Continue is literally impossible so this isn't necessary
	mod.SaveSaveData()
end]]

function mod.WipePersistentSaveData(skip_completion, skip_load)
	mod.savedata = {
		speed_mode = 2,
		position_indicator = 1,
		pickup_magnet = 1,
		challenge_mode = "none",

		beat_pillar_of_salt = false,
		beat_cardiac_arrest = false,
		beat_bound_by_steel = false,

		skin_a = "default",
		skin_b = "default",
		unlocked_tainted = false,

		beat_basement	= false,
		beat_caves		= false,
		beat_depths		= false,
		beat_downpour	= false,
		beat_mines		= false,
		beat_mausoleum	= false,

		--	duo_birthright	= false, "Camaraderie"

		cityboy = false,
		cityboytwo = false,
	}

	if not skip_completion then
		mod.InitCharacterCompletion("Sodom", false)
		mod.InitCharacterCompletion("Sodom", true, true)
	end

	mod.SaveSaveData()

	if not skip_load then
		mod.LoadSaveData()
	end
end

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, player)
	if game:GetNumPlayers() == 1 then
		mod.LoadSaveData()
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, fromsave)
	mod.LoadSaveData()
	mod.GameLoaded = true

	local choices = {"none"}

	if mod.savedata.beat_pillar_of_salt then
		table.insert(choices, "pillar of salt")
	end

	if mod.savedata.beat_cardiac_arrest then
		table.insert(choices, "cardiac arrest")
	end

	if mod.savedata.beat_bound_by_steel then
		table.insert(choices, "bound by steel")
	end

	mod.DeadSeaScrollsDirectory.settings.buttons[7].choices = choices
end)

mod:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, function(_, shouldsave)
	mod.SaveSaveData()
	mod.GameLoaded = false
end)

mod:AddCallback(ModCallbacks.MC_POST_GAME_END, function(_, died)
	mod.SaveSaveData()
	mod.GameLoaded = false
end)