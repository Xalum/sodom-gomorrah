local mod = XalumMods.SodomAndGomorrah

mod.SodomGomorrahCardBlacklist = {
	Card.CARD_SOUL_LOST,
}

function mod.IsCardBlacklisted(card)
	for _, value in pairs(mod.SodomGomorrahCardBlacklist) do
		if value == card then return true end
	end
	
	return false
end

mod:AddCallback(ModCallbacks.MC_GET_CARD, function(_, rng, card, can_suit, can_rune, force_rune)
	if mod.GameHasPlayerSodomGomorrah() and mod.IsCardBlacklisted(card) and not mod.AntiCardRecursion then
		mod.AntiCardRecursion = true

		local itempool = game:GetItemPool()
		local new
		local i = 0

		repeat
			i = i + 1
			new = itempool:GetCard(rng:GetSeed() + i, can_suit, can_rune, force_rune)
		until not mod.IsCardBlacklisted(new)

		mod.AntiCardRecursion = false

		return new
	end
end)