local mod = XalumMods.SodomAndGomorrah

local paused
local pausedAt
local pauseDuration = 0
local forceUnpause

function mod.PauseGame(frames)
	if game:GetRoom():GetBossID() ~= 54 then -- Intenionally fail on Lamb, since it breaks the Victory Lap menu super hard
		pausedAt = pausedAt or game:GetFrameCount()
		pauseDuration = pauseDuration + frames
		paused = true

		Isaac.GetPlayer():UseActiveItem(CollectibleType.COLLECTIBLE_PAUSE, UseFlag.USE_NOANIM)
	end
end

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if pausedAt and pausedAt + pauseDuration < game:GetFrameCount() then
		paused = false
		pausedAt = nil
		pauseDuration = 0

		forceUnpause = true
	end
end)

for hook = InputHook.IS_ACTION_PRESSED, InputHook.IS_ACTION_TRIGGERED do
	mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
		if paused and action ~= ButtonAction.ACTION_CONSOLE then
			return false
		elseif forceUnpause and action == ButtonAction.ACTION_SHOOTDOWN then
			forceUnpause = false
			return true
		end
	end, hook)
end

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if paused and action ~= ButtonAction.ACTION_CONSOLE then
		return 0
	end
end, InputHook.GET_ACTION_VALUE)

local achievementSprite = Sprite()
local achievementUpdate = false
local renderAchievement = false

achievementSprite:Load("gfx/ui/achievement/sg_achievement.anm2", true)

local achievementNoteQueue = {}
function mod.QueueAchievementNote(gfx)
	table.insert(achievementNoteQueue, gfx)
end

function mod.PlayAchievementNote(gfx)
	-- My pause function breaks S&G's chain, so for now it just won't pause
	-- mod.PauseGame(41)

	achievementSprite:ReplaceSpritesheet(2, gfx)
	achievementSprite:LoadGraphics()
	achievementSprite:Play("Idle", true)

	achievementUpdate = false
	renderAchievement = true

	sfx:Play(SoundEffect.SOUND_CHOIR_UNLOCK)
end

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if not renderAchievement and #achievementNoteQueue > 0 then
		mod.PlayAchievementNote(achievementNoteQueue[1])
		table.remove(achievementNoteQueue, 1)
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	if renderAchievement then
		if achievementUpdate then
			achievementSprite:Update()
		end
		achievementUpdate = not achievementUpdate
	
		local position = Vector(Isaac.GetScreenWidth() / 2, Isaac.GetScreenHeight() / 2)
		achievementSprite:Render(position, Vector.Zero, Vector.Zero)

		if achievementSprite:IsFinished() then
			renderAchievement = false
		end
	end
end)