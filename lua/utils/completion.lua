local mod = XalumMods.SodomAndGomorrah
local playertype_cache = {}

local DifficultyToCompletionMap = {
	[Difficulty.DIFFICULTY_NORMAL]	 = 1,
	[Difficulty.DIFFICULTY_HARD]	 = 2,
	[Difficulty.DIFFICULTY_GREED]	 = 1,
	[Difficulty.DIFFICULTY_GREEDIER] = 2,
}

local BossID = { -- Only the relevant ones
	HEART 		= 8,
	SATAN 		= 24,
	IT_LIVES	= 25,
	ISAAC		= 39,
	BLUE_BABY	= 40,
	LAMB		= 54,
	MEGA_SATAN	= 55,
	GREED		= 62,
	HUSH		= 63,
	DELIRIUM	= 70,
	GREEDIER	= 71,
	MOTHER		= 88,
	MAUS_HEART	= 90,
	BEAST		= 100,
}

function mod.InitCharacterCompletion(playername, taintedcompletion, tainted)
	local lookup = string.lower(playername)
	if tainted then lookup = lookup .. "b" end

	taintedcompletion = taintedcompletion and true or false
	mod.savedata.completion = mod.savedata.completion or {}

	playertype_cache[lookup] = Isaac.GetPlayerTypeByName(playername, tainted)

	if not mod.savedata.completion[lookup] then
		mod.savedata.completion[lookup] = {
			lookupstr	= lookup,
			istainted	= taintedcompletion,

			heart	= 0,
			isaac	= 0,
			bbaby	= 0,
			satan	= 0,
			lamb	= 0,
			rush	= 0,
			hush	= 0,
			deli	= 0,
			mega	= 0,
			greed	= 0,
			mother	= 0,
			beast	= 0,
		}
	end
end

mod:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function()
	if game.Challenge == 0 then
		local room = game:GetRoom()
		local roomtype = room:GetType()

		local value = DifficultyToCompletionMap[game.Difficulty]
		local check

		for _, data in pairs(mod.savedata.completion) do
			if playertype_cache[data.lookupstr] == Isaac.GetPlayer():GetPlayerType() then
				check = data
				break
			end
		end

		if check then
			if roomtype == RoomType.ROOM_BOSS then
				local boss = room:GetBossID()

				if game:GetLevel():GetStage() == LevelStage.STAGE7 then -- Void
					if boss == BossID.DELIRIUM then
						check.deli = math.max(check.deli, value)	
					end
				else
					if boss == BossID.HEART or boss == BossID.IT_LIVES or boss == BossID.MAUS_HEART then
						check.heart = math.max(check.heart, value)
					elseif boss == BossID.ISAAC then
						check.isaac = math.max(check.isaac, value)
					elseif boss == BossID.BLUE_BABY then
						check.bbaby = math.max(check.bbaby, value)
					elseif boss == BossID.SATAN then
						check.satan = math.max(check.satan, value)
					elseif boss == BossID.LAMB then
						check.lamb = math.max(check.lamb, value)
					elseif boss == BossID.HUSH then
						check.hush = math.max(check.hush, value)
					elseif boss == BossID.MEGA_SATAN then
						check.mega = math.max(check.mega, value)
					elseif boss == BossID.GREED or boss == BossID.GREEDIER then
						check.greed = math.max(check.greed, value)
					elseif boss == BossID.MOTHER then
						check.mother = math.max(check.mother, value)
					end
				end
			elseif roomtype == RoomType.ROOM_BOSSRUSH then
				check.rush = math.max(check.rush, value)
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, function(_, npc)
	if npc.Variant == 0 then -- The Beast
		local value = DifficultyToCompletionMap[game.Difficulty]
		local check

		for _, data in pairs(mod.savedata.completion) do
			if playertype_cache[data.lookupstr] == Isaac.GetPlayer():GetPlayerType() then
				check = data
				break
			end
		end

		if check then
			check.beast = math.max(check.beast, value)
		end
	end
end, 951)