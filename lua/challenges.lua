local mod = XalumMods.SodomAndGomorrah

function mod.IsNewCityBoyChapter()
	if mod.LastFloor.Stage < LevelStage.STAGE4_3 then
		return mod.LastFloor.Stage % 2 == 0
	end
end

function mod.ResetStageTracker()
	mod.CurrentFloor	= {Stage = 1, IsAlt = false, Damaged = false}
	mod.LastFloor		= {Stage = 1, IsAlt = false, Damaged = false}
	mod.LastLastFloor	= {Stage = 1, IsAlt = false, Damaged = false}
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	local level = game:GetLevel()

	local stage = level:GetStage()
	local stagetype = level:GetStageType()
	local repentance = stagetype == StageType.STAGETYPE_REPENTANCE or stagetype == StageType.STAGETYPE_REPENTANCE_B

	if stage == LevelStage.STAGE1_1 or not mod.CurrentFloor then
		mod.ResetStageTracker()
	elseif stage == mod.CurrentFloor.Stage then
		return
	end

	mod.LastLastFloor.Stage = mod.LastFloor.Stage
	mod.LastLastFloor.IsAlt = mod.LastFloor.IsAlt
	mod.LastLastFloor.Damaged = mod.LastFloor.Damaged

	mod.LastFloor.Stage = mod.CurrentFloor.Stage
	mod.LastFloor.IsAlt = mod.CurrentFloor.IsAlt
	mod.LastFloor.Damaged = mod.CurrentFloor.Damaged

	mod.CurrentFloor.Stage = stage
	mod.CurrentFloor.IsAlt = repentance
	mod.CurrentFloor.Damaged = false

	if Isaac.GetPlayer():GetPlayerType() == mod.PLAYER_TYPE.SODOM and game.Difficulty <= Difficulty.DIFFICULTY_HARD and game.Challenge == 0 then
		if mod.LastLastFloor.Stage == LevelStage.STAGE1_1 and mod.LastFloor.Stage == LevelStage.STAGE1_2 then -- Chapter 1
			if not (mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt) then -- Basement
				mod.savedata.beat_basement = true
			end
		elseif mod.LastLastFloor.Stage == LevelStage.STAGE2_1 and mod.LastFloor.Stage == LevelStage.STAGE2_2 then -- Chapter 2
			if not (mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt) then -- Caves
				mod.savedata.beat_caves = true
			end
		elseif mod.LastLastFloor.Stage == LevelStage.STAGE3_1 and mod.LastFloor.Stage == LevelStage.STAGE3_2 then -- Chapter 3
			if not (mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt) then -- Depths
				mod.savedata.beat_depths = true
			end
		end

		if mod.IsNewCityBoyChapter() and not mod.LastFloor.Damaged and not mod.LastLastFloor.Damaged then
			mod.savedata.cityboy = true
		end
	end

	if Isaac.GetPlayer():GetPlayerType() == mod.PLAYER_TYPE.SODOM_B and game.Difficulty <= Difficulty.DIFFICULTY_HARD and game.Challenge == 0 then
		if mod.LastLastFloor.Stage == LevelStage.STAGE1_1 and mod.LastFloor.Stage == LevelStage.STAGE1_2 then -- Chapter 1
			if mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt then -- Downpour
				mod.savedata.beat_downpour = true
			end
		elseif mod.LastLastFloor.Stage == LevelStage.STAGE2_1 and mod.LastFloor.Stage == LevelStage.STAGE2_2 then -- Chapter 2
			if mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt then -- Mines
				mod.savedata.beat_mines = true
			end
		elseif mod.LastLastFloor.Stage == LevelStage.STAGE3_1 and mod.LastFloor.Stage == LevelStage.STAGE3_2 then -- Chapter 3
			if mod.LastLastFloor.IsAlt and mod.LastFloor.IsAlt then -- Mausoleum
				mod.savedata.beat_mausoleum = true
			end
		end

		if mod.IsNewCityBoyChapter() and not mod.LastFloor.Damaged and not mod.LastLastFloor.Damaged then
			mod.savedata.cityboytwo = true
		end
	end

	if level:GetCurses() & LevelCurse.CURSE_OF_LABYRINTH > 0 then
		mod.CurrentFloor.Stage = mod.CurrentFloor.Stage + 1

		if stage ~= LevelStage.STAGE1_1 then
			mod.LastLastFloor.Stage = mod.LastLastFloor.Stage + 1
			mod.LastLastFloor.IsAlt = mod.LastFloor.IsAlt
			mod.LastLastFloor.Damaged = mod.LastLastFloor.Damaged

			mod.LastFloor.Stage = mod.LastFloor.Stage + 1
			mod.LastFloor.IsAlt = mod.CurrentFloor.IsAlt
			mod.LastFloor.Damaged = false
		end
	end
end)

--[[mod:AddCallback(ModCallbacks.MC_POST_CURSE_EVAL, function()
	return LevelCurse.CURSE_OF_LABYRINTH
end)]]