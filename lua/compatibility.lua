local mod = XalumMods.SodomAndGomorrah

-- Blacklists
--[[
	- Items blacklisted for the normal characters are also blacklisted for the tainted characters, but not vice versa
]]

function mod.AddBlacklistedSodomGomorrahItems(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddBlacklistedSodomGomorrahItems() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SodomAndGomorrahItemBlacklist, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddBlacklistedSodomGomorrahItems() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

function mod.AddBlacklistedSodomGomorrahTrinkets(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddBlacklistedSodomGomorrahTrinkets() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SodomAndGomorrahTrinketBlacklist, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddBlacklistedSodomGomorrahTrinkets() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

function mod.AddBlacklistedTaintedSodomGomorrahItems(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddBlacklistedTaintedSodomGomorrahItems() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SodomAndGomorrahItemBlacklistB, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddBlacklistedTaintedSodomGomorrahItems() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end


-- "Reviver" items are granted to both characters on pickup
function mod.AddSodomGomorrahReviverItems(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.RegisterSodomGomorrahReviverItems() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SodomAndGomorrahRevivers, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.RegisterSodomGomorrahReviverItems() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

-- Sodom-only items are transferred to Sodom if picked up as Gomorrah
function mod.AddSodomGomorrahSodomOnlyItems(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSodomOnlyItems() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SodomGomorrahMainOnlyItems, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSodomOnlyItems() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

-- Sodom-only active items cannot be used by Gomorrah by holding ctrl
function mod.AddSodomGomorrahSodomOnlyActiveItems(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSodomOnlyActiveItems() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.BannedGomorrahActives, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSodomOnlyActiveItems() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

-- Forced-Gomorrah actives are used by both players simultaneously, either for functionality or fun (e.g. Ponies, Berserk, and Plan C)
function mod.AddSodomGomorrahForcedGomorrahActives(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahForcedGomorrahActives() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.ForcedGomorrahActives, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahForcedGomorrahActives() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end

-- Single use active items are an extension of the Sodom-only active item table, separated in the hope that I can, in the future, get them to work as Gomorrah
function mod.AddSodomGomorrahSingleUseActives(val)
	if type(val) == "number" then
		val = {val}
	end

	if type(val) == "table" then
		for _, v in pairs(val) do -- Validation
			if type(v) ~= "number" then
				error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSingleUseActives() passed table containing non-number variables!", 2)
				return
			end
		end

		for _, v in pairs(val) do
			table.insert(mod.SingleUseActives, v)
		end
		return
	end

	error("Function XalumMod.SodomAndGomorrah.AddSodomGomorrahSingleUseActives() passed incorrect data type, expecting number or table, got " .. type(val) .. "!", 2)
end